#ifndef _ORGANISATION2_H_
#define _ORGANISATION2_H_

#include "init.h"
#include "dynamics.h"


#define O2_DR 60 
#define O2_DE 40 


//************************

// The splitting of the system can be conceived in several ways.

// Organization O2: The restaurant is only concerned by its attractivity. One control u3 is available. Agricultural activity is concerned with keeping a good soil quality: x3 >=x3QUAL; but also with keeping positive cumulative cash flows: x1>=0. 2 controls can be used, U1 and u2. 
// Negotiations between the two activities are about commitments on yearly crop production volume Rn_x3_u1 >= Rmin ; and a minimal gain from the restaurant Emin. 
// The aim is to compute the guaranteed viability kernels of both activities for various commitments, in other words various values for Rmin and Emin.


//************************

// Computation of the farmer's guaranteed viability kernel: 2 state dimensions x1 and x3, two control dimensions u1 and u2, 1 tyche Emin and two constraints plus one commitment. 

int O2_IsinConstraintSetAgricole2D(float x1, float x3);



int O2_IsinAdmissibleControlSetAgricole2D(float x1, float x3,int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2,float Rmin);

// x3 dynamics:
float O2_fx3next(float x3,int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2,float x3min,float x3max);

void O2_CalculNoyauAgricole2D(int Intx1x3frontiere[DX3],float Rmin,float Emin,float Emax, int DE);

// Computation of the restaurant's viability kernel: 1 state dimension x2, 1 control u3, one tyche R and no constraint but one commitment. 

int O2_IsinConstraintSetREstaurant1D(float x2);

int O2_IsinAdmissibleControlSetRestaurant1D(float x2,float u3, float Rmin, float Emin);

//Dynamics of x1 and x2

float O2_fx1next(float x1,float u1, float u2,float E,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12]);

float O2_fx2next(float x2,float u3,float Rn_x3_u1);


void O2_CalculFrontiereNoyauRestaurant1D(int Intx2frontiere[DX2],float Rmax,float Rmin,float Emax,float Emin,int DR);


//Computation of the kernel's size
float O2_CalculSurfacenoyauAgricole2D(int Intx1x3frontiere[DX3]);

float O2_CalculSurfacenoyauRestaurant1D(int Intx2frontiere[DX2]);

float O2_CalculSurfacenoyauProduit(int Intx1x3frontiere[DX3], int Intx2frontiere[DX2]);



#endif
