#include <math.h>
#include <stdio.h>
#include "dynamics.h"
#include "utils.h"


//Computing x1 dynamics

//Evolution of the restaurant's gains

float GainRepas(float x2,float u3){
    return NB_CLIENTS_MAX*x2*u3*GAIN;
}

float CoutAchatFoodExterieur(float x2,float Rn_x3_u1){
	float c;
	float food_quant;
	///Computes expenses linked to food bought from other producers.
	food_quant = NB_CLIENTS_MAX*x2*FPP;
	c = fmaxf(food_quant-Rn_x3_u1,0)*FOOD_PRICE;
	return c;
}


//Agricultural expenses



float Cout(int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2){
    int i;
    float expenses=0.0;
    for (i=NB_CROPS;i<2*NB_CROPS;i++){
        expenses = expenses +Assolements[u1][i];
    }
    expenses = expenses*10000 ; // €/m² become €/ha
    return expenses* u2 / NB_CROPS + FIXED_COSTSA;
}


//Computing x2 dynamics

// Evolution of the attractiveness coefficient


float Attractiveness_evolution(float x2,float u3,float Rn_x3_u1){
    float x2next,newx2,u3mid;
    // Evolution based on the price: the cheaper the meal, the happier the customer (with a neutral effect for a mean price)
    u3mid = U3MIN + (U3MAX-U3MIN)/2;
    newx2 = x2 + (u3mid-u3) / u3mid;
    // Evolution based on the use of locally-produced vegetables: the higher the share of the meal is locally produced, the higher clients' satisfaction is 

    newx2 = newx2 + (Rn_x3_u1/(NB_CLIENTS_MAX*x2*FPP)-R_MID); 
    x2next = fmaxf(X2MIN, fminf(X2MAX,newx2));
    return x2next;
}

//Computing x3 dynamics


float R_n(float x3,float RM,float ri){
        float Rn;
        /// Function returning the yield (kg/m2) for a crop characterized by TM and ri and a soil quality x3
        if (x3==0.0) Rn = 0.0;
        else if (ri!=0.5){
            if (x3<0.5){

                Rn = 2*RM*(x3*(1-2*ri)+ri-sqrtf(ri*ri+2*x3*(1-2*ri)))/(2*ri-1);
            }
            else {
                Rn = RM*(1+2*ri*((1-2*ri)*(x3-0.5)-(1-ri)+sqrtf((1-ri)*(1-ri)+2*(x3-0.5)*(2*ri-1)))/(2*ri-1));

            }

       if(Rn<0){
       	printf("Negative Rn (1)\n");
       	printf("Rm x3 ri Rn %f %f %f %f\n",RM,x3,ri,Rn);

       	getchar();
       }
	else if(isnan(Rn)){
       	printf("Rn nan (1)");
       	printf("Rm ri Rn %f %f %f %f\n",RM,ri,Rn,ri*ri+2*x3*(1-2*ri));

       	getchar();
       }

        }
        else {
            if (x3<0.5){
                Rn = 2*RM*x3;
            }
            else{
                Rn = RM*(x3+0.5);
           }
       if(Rn<0){
       	printf("Negative Rn (2)");
       	printf("Rm ri %f %f\n",RM,ri);
       	getchar();
       }

       }
       return Rn;
}






//fonction qui donne la production d'une rotation de nb_crops especes distinctes caractérisées chacune par TM et ri et une qualité du sol x3 et la surface u2


float R_nS(float x3, int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2){
    ///u2 in hectares, R_nS in kg
    /// Function returning the yield of a rotation made of nb_crops different species, each one being characterized by TM and ri, a soil quality x3 and a surface u2
    int i,j;
    float rwnum,rwden; // relative_work numerator / denominator
    float RnS=0;

    for (i=0;i<NB_CROPS;i++){
            RnS = RnS+R_n(x3,Assolements[u1][i],Assolements[u1][i+2*NB_CROPS]);
    }
    RnS = RnS*10000;// m2 become hectares
    RnS = RnS*u2/NB_CROPS;
    rwnum = 0;
    rwden = 0;
    for (j=0;j<12;j++){
            rwnum = rwnum+fminf(THR_WORKLOAD,Assolements[u1][6*NB_CROPS+j]*u2);
            rwden = rwden+Assolements[u1][6*NB_CROPS+j]*u2;
    }
    if (rwden>0) {
	    RnS = RnS*rwnum/rwden;
    }

    return RnS*(1-WASTE);
}



float production_maximale(){
	float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12];
	float rcurrent,rres;
	int i,j,b;
    /// Function computing the maximal yield for agricultural production

	b = CreateAssolements(Assolements);
	rres = 0;
	for(i=0;i<NB_ASSOLEMENTS;i++) {
		for(j=0;j<DU2;j++) {
			rcurrent = R_nS(X3MAX,i,Assolements,Valeur(j,U2MAX,U2MIN,DU2));

			if (rres < rcurrent) rres = rcurrent;
		}
	}
	return rres;
}

float cout_maximal(){
	float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12];
	float ecurrent,eres;
	int i,j,b;
	b = CreateAssolements(Assolements);
	eres = 0;
	for(i=0;i<NB_ASSOLEMENTS;i++) {
		for(j=0;j<DU2;j++) {
			ecurrent = Cout(i,Assolements,Valeur(j,U2MAX,U2MIN,DU2));
			if (eres < ecurrent) eres = ecurrent;
		}
	}
	return eres;
}


float gain_maximal(float Rmax){
	float ecurrent,eres,x2min,x2max,x2,u3,u3min,u3max;
	int i,j;
	eres = 0;

	x2min = fx2min();
	x2max= fx2max();
    u3min = fu3min();
	u3max = fu3max();

	for(i=0;i<DX2;i++) {
            x2 = Valeur(i,x2max,x2min,DX2);
		for(j=0;j<DU3;j++) {
            u3 = Valeur(j,u3max,u3min,DX3);
			ecurrent = GainRepas(x2,u3)-CoutAchatFoodExterieur(x2,Rmax)-FIXED_COSTSR;
			if (eres < ecurrent) eres = ecurrent;
		}
	}
	return eres;
}




float BISQ_evol(float x3, int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2,float x3min,float x3max){
    /// Evolution of soil quality index
	float dIb = 0;
	float dIp = 0;
	float yn;
	int i;
	for (i=0;i<NB_CROPS;i++){
        yn = R_n(x3,Assolements[u1][i],Assolements[u1][2*NB_CROPS+i]);
		dIb = dIb + yn/(2*Assolements[u1][i])*Assolements[u1][3*NB_CROPS+i]*u2 / NB_CROPS;
		dIp = dIp + (x3 * Assolements[u1][4*NB_CROPS+i] + Assolements[u1][5*NB_CROPS+i])*u2/NB_CROPS;
	}
	return fminf(fmaxf(x3 - dIb + dIp+RAP_FALLOW*(1-u2),x3min),x3max);
}

