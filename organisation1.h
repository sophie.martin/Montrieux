#ifndef _ORGANISATION1_H_
#define _ORGANISATION1_H_

#include "init.h"
#include "dynamics.h"


#define O1_DR 60 
#define O1_DE 40 

//************************
// The splitting of the system can be conceived in several ways.

// Organization O1: Farm activity is only concerned by keeping good soil quality: x3 >=x3QUAL. Only x3 can be seen and 2 controls can be used, U1 and u2. The restaurant is concerned by keeping global cumulative cash flows positive: x1>=0 using control u3
// Negotiations between the two activities are about commitments on yearly crop production volume Rn_x3_u1 >= Rmin ; and a maximal cost for this production expenses<=Emax.
// The aim is to compute the guaranteed viability kernels of both activities for various commitments, in other words various values for Rmin and Emax.

//************************


// Computation of the farmer's guaranteed viability kernel: 1 state dimension x3, two control dimensions u1 and u2, one constraint plus two commitments. 

// Verification that x3 respects the constraint.
int O1_IsinConstraintSetAgricole1D(float x3);

// This function verifies that the couple of controls (u1, u2) is allowed with a state of the system x3 given the two commitments made.
int O1_IsinAdmissibleControlSetAgricole1D(float x3,int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2,float Rmin,float Emax);

// x3 dynamics:
float O1_fx3next(float x3,int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2,float x3min,float x3max);

void O1_CalculNoyauAgricole1D(int Intx3frontiere[DX3],float Rmin,float Emax);

// Computation of the restaurant's guaranteed viability kernel: 2 state dimensions x1 and x2, 1 control u3, one constraint, no commitment. 

int O1_IsinConstraintSetREstaurant2D(float x1,float x2);

int O1_IsinAdmissibleControlSetRestaurant2D(float x1,float x2,float u3);

//Dynamics of x1 and x2

float O1_fx1next(float x1,float x2,float u3,float Rn_x3_u1,float E);

float O1_fx2next(float x2,float u3,float Rn_x3_u1);


// Computation of the lower border of the guaranteed viability kernel as a table Intx2frontiere[DX2] associating attractivity values (from X2MIN to X2MAX) with the index of the lowest value of cumulative cash flow x1 belonging to the guaranteed viability kernel.
// Here, the property of linearity with x1 is used to transform a 2D-kernel into a 1D-table.

void O1_CalculFrontiereNoyauRestaurant2D(int Intx1frontiere[DX2],float Rmax,float Rmin,float Emax,float Emin,int DR,int DE);


//Computation of the kernel size
float O1_CalculSurfacenoyauAgricole1D(int Intx3frontiere[DX3]);

float O1_CalculSurfacenoyauRestaurant2D(int Intx1frontiere[DX2]);

float O1_CalculSurfacenoyauProduit(int Intx3frontiere[DX3], int Intx1frontiere[DX2]);


// Trajectories

int O1_CalculTrajectoireViable(float x10,float x20,float x30, int Intx3frontiere[DX3],int Intx1frontiere[DX2],float traj_x1[TMAX],float traj_x2[TMAX],float traj_x3[TMAX],int traj_u1[TMAX],float traj_u2[TMAX],float traj_u3[TMAX]);


void shuffle_ctrls(int u1[NB_ASSOLEMENTS], int u2[DU2], int u3[DU3]);

int O1_CalculTrajectoireViableAlea(float x10,float x20,float x30, int Intx3frontiere[DX3],int Intx1frontiere[DX2],float traj_x1[TMAX],float traj_x2[TMAX],float traj_x3[TMAX],int traj_u1[TMAX],float traj_u2[TMAX],float traj_u3[TMAX]);



#endif
