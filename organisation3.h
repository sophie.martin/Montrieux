#ifndef _ORGANISATION3_H_
#define _ORGANISATION3_H_

#include "init.h"
#include "dynamics.h"


#define O3_DR 60 
#define O3_DE 40 






int O3_IsinConstraintSetAgricole2D(float x1, float x3);


int O3_IsinAdmissibleControlSetAgricole2D(float x1, float x3,int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2,float Rmin);

// x3 dynamics:
float O3_fx3next(float x3,int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2,float x3min,float x3max);

void O3_CalculNoyauAgricole2D(int Intx1x3frontiere[DX3],float Rmin,float Emin,float Emax, int DE);


int O3_IsinConstraintSetREstaurant2D(float x1,float x2);

int O3_IsinAdmissibleControlSetRestaurant2D(float x1,float x2,float u3, float R, float Emin);

//Dynamics of x1 and x2

float O3_fx1Anext(float x1,float u1, float u2,float E,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12]);

float O3_fx1Rnext(float x1, float x2, float u3, float Rn_x3_u1, float Emin);

float O3_fx2next(float x2,float u3,float Rn_x3_u1);



void O3_CalculFrontiereNoyauRestaurant2D(int Intx1frontiere[DX2],float Rmax,float Rmin,float Emin,int DR, int DE);


//Computation of the kernel's size
float O3_CalculSurfacenoyauAgricole2D(int Intx1x3frontiere[DX3]);

float O3_CalculSurfacenoyauRestaurant2D(int Intx2frontiere[DX2]);

float O3_CalculSurfacenoyauProduit(int Intx1x3frontiere[DX3], int Intx2frontiere[DX2]);



#endif

