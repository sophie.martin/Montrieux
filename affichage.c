#include <stdio.h>
#include "affichage.h"
#include "init.h"
#include "utils.h"
#include "dynamics.h"
#include "organisation1.h"
#include "organisation2.h"
#include "organisation3.h"
#include "organisationU.h"
#include "affichage.h"



void InscriptionParametres(FILE *f){
	/// Function saving the simulation's parameters in a file.

        fprintf(f,"VERSION : %s\n",VERSION);
        fprintf(f,"Parameters for the dynamics: \n");
        fprintf(f,"Gain : %f\n",GAIN);
        fprintf(f,"Fixed_costs_R : %d\n",FIXED_COSTSR);
        fprintf(f,"Food_price : %f\n",FOOD_PRICE);
        fprintf(f,"Price_Mtrx : %f\n",PRICE_MTX);
        fprintf(f,"Fixed_costs_A : %d\n",FIXED_COSTSA);
        fprintf(f,"Nb clients max : %d\n",NB_CLIENTS_MAX);
        fprintf(f,"Food per plate : %f\n",FPP);
        fprintf(f,"Workload threshold : %d\n",THR_WORKLOAD);
        fprintf(f,"Waste : %f\n",WASTE);
        fprintf(f,"Rmid : %f\n",R_MID);
        fprintf(f,"Threshold for soil quality (viable minimum) : %f\n",X3QUAL);
        fprintf(f,"\n");
	fprintf(f,"Parameters for the discretization: \n");
	fprintf(f,"x1max %f ",fx1max());
	fprintf(f,"x1min %f ",fx1min());
	fprintf(f,"DX1 %d\n",DX1);
	fprintf(f,"x2max %f ",fx2max());
	fprintf(f,"x2min %f ",fx2min());
	fprintf(f,"DX2 %d\n",DX2);
	fprintf(f,"x3max %f ",fx3max());
	fprintf(f,"x3min %f ",fx3min());
	fprintf(f,"DX3 %d\n",DX3);
	fprintf(f,"u2max %f ",fu2max());
	fprintf(f,"u2min %f ",fu2min());
	fprintf(f,"DU2 %d\n",DU2);

	fprintf(f,"u3max %f ",fu3max());
	fprintf(f,"u3min %f ",fu3min());
	fprintf(f,"DU3 %d\n",DU3);
	fprintf(f,"Rmax %f ",production_maximale());
	fprintf(f,"DR %d \n",O1_DR);
	fprintf(f,"Emax %f ",cout_maximal());
	fprintf(f,"DE %d \n",O1_DE);
	fprintf(f,"\n");
}



void O1_InscriptionResultats(FILE *f, float surfaces_Agricole1D[O1_DR][O1_DE], float surfaces_Restaurant2D[O1_DR][O1_DE], float surfaces_produit[O1_DR][O1_DE]){
	/// Saving the result for simulations using organization O1.
    float Rmin, Emax;
    int i, j;

    float RMAX = production_maximale();
	float EMAX = cout_maximal();

    for (i=0;i<O1_DR;i++){
		if (i==0) fprintf(f,"[");
		else fprintf(f,",");
		Rmin = Valeur(i,RMAX,0.0,O1_DR);
		fprintf(f,"%f",Rmin);
		if (i==O1_DR-1) fprintf(f,"]\n");
	}
	fprintf(f,"\n");
	for (i=0;i<O1_DE;i++){
		if (i==0) fprintf(f,"[");
		else fprintf(f,",");
		Emax = Valeur(i,EMAX,0.0,O1_DE);
		fprintf(f,"%f",Emax);
		if (i==O1_DE-1) fprintf(f,"]\n");
	}
	fprintf(f,"\n RESTAURANT\n");
	for (i=0;i<O1_DE;i++){
		if (i==0) fprintf(f,"[");
		else fprintf(f,",");
		for (j=0;j<O1_DR;j++){
			if (j==0) fprintf(f,"[");
			else fprintf(f,",");
			fprintf(f,"%f",surfaces_Restaurant2D[j][i]);
			if (j==O1_DR-1) fprintf(f,"]");
		}
		if (i==O1_DE-1) fprintf(f,"]\n");
	}

	fprintf(f,"\n AGRICULTURE\n");
	for (i=0;i<O1_DE;i++){
		if (i==0) fprintf(f,"[");
		else fprintf(f,",");
		for (j=0;j<O1_DR;j++){
			if (j==0) fprintf(f,"[");
			else fprintf(f,",");
			fprintf(f,"%f",surfaces_Agricole1D[j][i]);
			if (j==O1_DR-1) fprintf(f,"]");
		}
		if (i==O1_DE-1) fprintf(f,"]\n");
    }

        fprintf(f,"\n PRODUCT\n");
	for (i=0;i<O1_DE;i++){
		if (i==0) fprintf(f,"[");
		else fprintf(f,",");
		for (j=0;j<O1_DR;j++){
			if (j==0) fprintf(f,"[");
			else fprintf(f,",");
			fprintf(f,"%f",surfaces_produit[j][i]);
			if (j==O1_DR-1) fprintf(f,"]");
		}
		if (i==O1_DE-1) fprintf(f,"]\n");
    }

    fprintf(f,"\n");
    fprintf(f,"\n");
    fprintf(f,"\n");
}




void O2_InscriptionResultats(FILE *f, float surfaces_Agricole2D[O2_DR][O2_DE], float surfaces_Restaurant1D[O2_DR][O2_DE],float surfaces_produit[O2_DR][O2_DE]){
	/// Saving the result for simulations using organization O2.
    float Rmin, Emin;
    int i, j;

    float RMAX = production_maximale();
	float EMAX = cout_maximal();

    for (i=0;i<O2_DR;i++){
		if (i==0) fprintf(f,"[");
		else fprintf(f,",");
		Rmin = Valeur(i,RMAX,0.0,O2_DR);
		fprintf(f,"%f",Rmin);
		if (i==O2_DR-1) fprintf(f,"]\n");
	}
	fprintf(f,"\n");
	for (i=0;i<O2_DE;i++){
		if (i==0) fprintf(f,"[");
		else fprintf(f,",");
		Emin = Valeur(i,EMAX,0.0,O2_DE);
		fprintf(f,"%f",Emin);
		if (i==O2_DE-1) fprintf(f,"]\n");
	}
	fprintf(f,"\n RESTAURANT\n");
	for (i=0;i<O2_DE;i++){
		if (i==0) fprintf(f,"[");
		else fprintf(f,",");
		for (j=0;j<O2_DR;j++){
			if (j==0) fprintf(f,"[");
			else fprintf(f,",");
			fprintf(f,"%f",surfaces_Restaurant1D[j][i]);
			if (j==O2_DR-1) fprintf(f,"]");
		}
		if (i==O2_DE-1) fprintf(f,"]\n");
	}

	fprintf(f,"\n AGRICULTURE\n");
	for (i=0;i<O2_DE;i++){
		if (i==0) fprintf(f,"[");
		else fprintf(f,",");
		for (j=0;j<O2_DR;j++){
			if (j==0) fprintf(f,"[");
			else fprintf(f,",");
			fprintf(f,"%f",surfaces_Agricole2D[j][i]);
			if (j==O2_DR-1) fprintf(f,"]");
		}
		if (i==O2_DE-1) fprintf(f,"]\n");
    }

        fprintf(f,"\n PRODUCT\n");
	for (i=0;i<O2_DE;i++){
		if (i==0) fprintf(f,"[");
		else fprintf(f,",");
		for (j=0;j<O2_DR;j++){
			if (j==0) fprintf(f,"[");
			else fprintf(f,",");
			fprintf(f,"%f",surfaces_produit[j][i]);
			if (j==O2_DR-1) fprintf(f,"]");
		}
		if (i==O2_DE-1) fprintf(f,"]\n");
    }

    fprintf(f,"\n");
    fprintf(f,"\n");
    fprintf(f,"\n");
}




void O3_InscriptionResultats(FILE *f, float surfaces_Agricole2D[O3_DR][O3_DE], float surfaces_Restaurant2D[O3_DR][O3_DE], float surfaces_produit[O3_DR][O3_DE]){
	/// Saving the result for simulations using organization O3.
    float Rmin, Emin;
    int i, j;

    float RMAX = production_maximale();
	float EMAX = cout_maximal();

    for (i=0;i<O3_DR;i++){
		if (i==0) fprintf(f,"[");
		else fprintf(f,",");
		Rmin = Valeur(i,RMAX,0.0,O3_DR);
		fprintf(f,"%f",Rmin);
		if (i==O3_DR-1) fprintf(f,"]\n");
	}
	fprintf(f,"\n");
	for (i=0;i<O3_DE;i++){
		if (i==0) fprintf(f,"[");
		else fprintf(f,",");
		Emin = Valeur(i,EMAX,0.0,O3_DE);
		fprintf(f,"%f",Emin);
		if (i==O3_DE-1) fprintf(f,"]\n");
	}
	fprintf(f,"\n RESTAURANT\n");
	for (i=0;i<O3_DE;i++){
		if (i==0) fprintf(f,"[");
		else fprintf(f,",");
		for (j=0;j<O3_DR;j++){
			if (j==0) fprintf(f,"[");
			else fprintf(f,",");
			fprintf(f,"%f",surfaces_Restaurant2D[j][i]);
			if (j==O3_DR-1) fprintf(f,"]");
		}
		if (i==O3_DE-1) fprintf(f,"]\n");
	}

	fprintf(f,"\n AGRICULTURE\n");
	for (i=0;i<O3_DE;i++){
		if (i==0) fprintf(f,"[");
		else fprintf(f,",");
		for (j=0;j<O3_DR;j++){
			if (j==0) fprintf(f,"[");
			else fprintf(f,",");
			fprintf(f,"%f",surfaces_Agricole2D[j][i]);
			if (j==O3_DR-1) fprintf(f,"]");
		}
		if (i==O3_DE-1) fprintf(f,"]\n");
    }

    fprintf(f,"\n PRODUCT\n");
	for (i=0;i<O3_DE;i++){
		if (i==0) fprintf(f,"[");
		else fprintf(f,",");
		for (j=0;j<O3_DR;j++){
			if (j==0) fprintf(f,"[");
			else fprintf(f,",");
			fprintf(f,"%f",surfaces_produit[j][i]);
			if (j==O3_DR-1) fprintf(f,"]");
		}
		if (i==O3_DE-1) fprintf(f,"]\n");
    }

    fprintf(f,"\n");
    fprintf(f,"\n");
    fprintf(f,"\n");
}


void InscriptionResultatsU(FILE *f, int Values[DX2][DX3]){
    int i, j;
    float surface_3D;

    surface_3D = CalculSurfacenoyau3D(Values);
    fprintf(f,"Surface : %f \n",surface_3D);
    fprintf(f,"\n 3D KERNEL: \n");

    for (i=0;i<DX2;i++){
        if (i==0) fprintf(f,"[");
        for (j=0;j<DX3;j++){
            if (j==0) fprintf(f,"[");
            else fprintf(f,",");
            fprintf(f,"%d", Values[i][j]);
            if (j==DX3-1) {
                fprintf(f,"]");
                if (i<DX2-1) fprintf(f,",");
        }}
        if (i==DX2-1) fprintf(f,"]");
    }
}
void Affichage_vecteur(FILE *f,float xmax,float xmin,int dx){
	int i;
	fprintf(f,"[");
	for (i=0;i<dx;i++){
		fprintf(f,"%f",Valeur(i,xmax,xmin,dx));
		if (i < dx-1) fprintf(f,",");
	}
	fprintf(f,"]\n");
}

void Affichage_Noyau_1D_x3(FILE *f,int Values[DX3]){
	int i;
	fprintf(f,"[");
	for (i=0;i<DX3;i++){
		fprintf(f,"%d",Values[i]);
		if (i < DX3-1) fprintf(f,",");
	}
	fprintf(f,"]\n");
}

void Affichage_Frontiere_Noyau_2D_x2x1(FILE *f,int Values[DX2]){
	int i;
	fprintf(f,"[");
	for (i=0;i<DX2;i++){
		fprintf(f,"%d",Values[i]);
		if (i < DX2-1) fprintf(f,",");
	}
	fprintf(f,"]\n");
}

void Affichage_Noyau_1D_x2(FILE *f,int Values[DX2]){
	int i;
	fprintf(f,"[");
	for (i=0;i<DX2;i++){
		fprintf(f,"%d",Values[i]);
		if (i < DX2-1) fprintf(f,",");
	}
	fprintf(f,"]\n");
}

void Affichage_Frontiere_Noyau_2D_x3x1(FILE *f,int Values[DX3]){
	int i;
	fprintf(f,"[");
	for (i=0;i<DX3;i++){
		fprintf(f,"%d",Values[i]);
		if (i < DX3-1) fprintf(f,",");
	}
	fprintf(f,"]\n");
}

void Affichage_Frontiere_Noyau_3D_x2x3x1(FILE *f,int Values[DX2][DX3]){
	int i,j;
	for (i=0;i<DX2;i++){
		if (i==0) fprintf(f,"[");
		for (j=0;j<DX3;j++){
			if (j==0) fprintf(f,"[");
			else fprintf(f,",");
			fprintf(f,"%d", Values[i][j]);
			if (j==DX3-1) {
				fprintf(f,"]");
				if (i<DX2-1) fprintf(f,",");
			}
		}
		if (i==DX2-1) fprintf(f,"]");
	}
}

void Affichage_Traj(FILE *f, float traj_x1[TMAX],float traj_x2[TMAX],float traj_x3[TMAX],int traj_u1[TMAX],float traj_u2[TMAX],float traj_u3[TMAX]){
    int i;

    fprintf(f, "T = %d\n", TMAX);

       fprintf(f,"traj_x1 = [");
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%f",traj_x1[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
    fprintf(f,"traj_x2 = [");
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%f",traj_x2[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
    fprintf(f,"traj_x3 = [");
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%f",traj_x3[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
    fprintf(f,"traj_u1 = [");
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%d",traj_u1[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
    fprintf(f,"traj_u2 = [");
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%f",traj_u2[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
    fprintf(f,"traj_u3 = [");
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%f",traj_u3[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
}

void Affichage_Traj_i(FILE *f, float traj_x1[TMAX],float traj_x2[TMAX],float traj_x3[TMAX],int traj_u1[TMAX],float traj_u2[TMAX],float traj_u3[TMAX],int n){
    int i;

       fprintf(f,"traj_x1_%d = [",n);
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%f",traj_x1[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
    fprintf(f,"traj_x2_%d = [",n);
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%f",traj_x2[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
    fprintf(f,"traj_x3_%d = [",n);
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%f",traj_x3[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
    fprintf(f,"traj_u1_%d = [",n);
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%d",traj_u1[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
    fprintf(f,"traj_u2_%d = [",n);
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%f",traj_u2[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
    fprintf(f,"traj_u3_%d = [",n);
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%f",traj_u3[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
}


void Affichage_pour_affichage_python(FILE *f,int Values_O1_x3[DX3],int Values_O1_x2x1[DX2],int Values_O2_x2[DX2],int Values_O2_x3x1[DX3],int Values_O3_x3x1[DX3],int Values_O3_x2x1[DX2],int ValuesU[DX2][DX3]){
	/// This function's outputs are formatted in order to be copied and pasted into the Jupyter Notebook.
	int i,j,k;
	fprintf(f,"# To be get from the *.c file. \n## About the grids: Grille_x1 table of size DX1; Grille_x2 table of size DX2 ; Grille_x3 table of size DX3\n");
	fprintf(f,"Grille_x1 = ");
	Affichage_vecteur(f,X1MAX,X1MIN,DX1);
	fprintf(f,"Grille_x2 = ");
	Affichage_vecteur(f,X2MAX,X2MIN,DX2);
	fprintf(f,"Grille_x3 = ");
	Affichage_vecteur(f,X3MAX,X3MIN,DX3);

	fprintf(f,"#About the kernels\n#Kernel O1 Agricultural 1D\n");
	fprintf(f,"O1_Noyau_x3 = ");
	Affichage_Noyau_1D_x3(f,Values_O1_x3);
	fprintf(f,"#Kernel O1 Restaurant 2D\n");
	fprintf(f,"O1_Frontiere_Noyau_x2x1 =");
	Affichage_Frontiere_Noyau_2D_x2x1(f,Values_O1_x2x1);

	fprintf(f,"\n#Kernel O2 Restaurant1D\n");
	fprintf(f,"O2_Noyau_x2 = ");
	Affichage_Noyau_1D_x2(f,Values_O2_x2);
	fprintf(f,"#Kernel O2 Agri 2D\n");
	fprintf(f,"O2_Frontiere_Noyau_x3x1 =");
	Affichage_Frontiere_Noyau_2D_x3x1(f,Values_O2_x3x1);

	fprintf(f,"\n#Kernel O3 Agricultural 2D\n");
	fprintf(f,"O3_Noyau_x3x1 = ");
	Affichage_Frontiere_Noyau_2D_x3x1(f,Values_O3_x3x1);
	fprintf(f,"#Kernel O3 Restaurant 2D\n");
	fprintf(f,"O3_Frontiere_Noyau_x2x1 =");
	Affichage_Frontiere_Noyau_2D_x2x1(f,Values_O3_x2x1);

	fprintf(f,"#Kernel Unified organization 3D\n");
	fprintf(f,"U_Frontiere_Noyau_x2x3x1 =");
	Affichage_Frontiere_Noyau_3D_x2x3x1(f,ValuesU);
}



void SaveKernel(FILE *f,int ValuesU[DX2][DX3]){
   fwrite(ValuesU, sizeof(int), DX2*DX3, f);
}


void LoadKernel(FILE *f,int ValuesU[DX2][DX3]){
    fread(ValuesU, sizeof(int), DX2*DX3, f);
}


void SaveTimes(FILE *f, float comp_times[O1_DR][O1_DE]){

    float Rmin, Emin;
    int i, j;

    float RMAX = production_maximale();
	float EMAX = cout_maximal();

	for (i=0;i<O1_DE;i++){
		if (i==0) fprintf(f,"[");
		else fprintf(f,",");
		for (j=0;j<O1_DR;j++){
			if (j==0) fprintf(f,"[");
			else fprintf(f,",");
			fprintf(f,"%f",comp_times[j][i]);
			if (j==O1_DR-1) fprintf(f,"]");
		}
		if (i==O1_DE-1) fprintf(f,"]\n");
	}
}
