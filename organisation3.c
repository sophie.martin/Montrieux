#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "organisation3.h"
#include "dynamics.h"
#include "utils.h"

//************************

// The splitting of the system can be conceived in several ways.

// Organization O3: Each activity has its own cumulative cash flow. They agree about a minimal agricultural production volume supplied to the restaurant, and a minimal amount the restaurant will be able to pay for this production.

// Agriculture: 
//- 2 states : x_{1A} (agricultural cumulative cash flow) et x_3 (soil quality)
//- 2 controls : u_1 (rotation) et u_2 (surface)
//- 1 tyche : E = available cash to pay for the vegetables (E >= Emin)
//- 2 constraints : x_{1A} >= 0 et x_3 >= x_{3min}
//- 1 commitment on the agricultural production Rmin
//
//Restaurant :
//- 2 states : x_{1R} (restaurant's cumulative cash flow) et x_2 (attractivity)
//- 1 control : u_3 (price of the meal)
//- 1 tyche : R = agricultural production (R >= Rmin)
//- 1 constraint : x_{1R} >= 0
//- 1 commitment : on the minimal amount of money available Emin

//************************



int O3_IsinConstraintSetAgricole2D(float x1, float x3){
	int b=0;
	if (x3>=X3QUAL && x1>=X1V) b = 1; // viable
	return b;
}


int O3_IsinAdmissibleControlSetAgricole2D(float x1, float x3,int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2,float Rmin){
	/// Function that verifies if the couple of controls (u1,u2) meets the requirements when the system's state is (x1,x3) given the commitment made.
	int b=0;
	if (R_nS(x3,u1,Assolements,u2)>=Rmin) b = 1;
	//printf("cout %f production %f\n",Cout(u1,Assolements,u2),R_nS(x3,u1,Assolements,u2));
	return b;
}

float O3_fx1Anext(float x1,float u1, float u2, float E,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12]){
	float x1next;
	x1next = x1+ E-Cout(u1,Assolements,u2);
	return x1next;
}

// x3 dynamics:
float O3_fx3next(float x3,int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2,float x3min,float x3max){
	float x3next;
	x3next = BISQ_evol(x3,u1,Assolements,u2,x3min,x3max);
	return x3next;
}

void O3_CalculNoyauAgricole2D(int Intx1x3frontiere[DX3],float Rmin,float Emin,float Emax,int DE){
	float x1min,x1max,x1, x3min,x3max, u2min,u2max, x3,x1next,x3next,u2,E;
	float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12];
	int change,passage,i,j,b,ii,jj,jjj,u1,intx3next;



	x1min = fx1min();
	x1max= fx1max();
	x3min = fx3min();
	x3max= fx3max();
	b = CreateAssolements(Assolements);
	u2min = fu2min();
	u2max = fu2max();

	change=1;
	passage = 1;


	for(i=0;i<DX3;i++) {
		x3= Valeur(i,x3max,x3min,DX3);
		if (O3_IsinConstraintSetAgricole2D(X1V,x3)) Intx1x3frontiere[i]=X1V;
		else Intx1x3frontiere[i]=DX1;
	}

//Computation
	while(change>0){
		change=0;
		passage = passage + 1;
		for(i=0;i<DX3;i++){
            x3= Valeur(i,x3max,x3min,DX3);
			for(j=Intx1x3frontiere[i];j<DX1;j++){
				b = 0;
				x1 = Valeur(j,x1max,x1min,DX1);
				
				if (O3_IsinConstraintSetAgricole2D(x1,x3)){
                    for(ii=0;ii<NB_ASSOLEMENTS;ii++) {
						u1 = ii;
					
                        for(jj=0;jj<DU2;jj++) {
                            u2 = Valeur(jj,u2max,u2min,DU2);
                            if (O3_IsinAdmissibleControlSetAgricole2D(x1,x3, u1, Assolements,u2,Rmin)){
                                b = 1;
                                x3next = O3_fx3next(x3,u1,Assolements,u2,x3min,x3max);
                                for(jjj=0;jjj<DE;jjj++) {
                                    E = Valeur(jjj,Emax,Emin,DE);
                                    x1next = O3_fx1Anext(x1,u1,u2,E,Assolements);
                                    if (O3_IsinConstraintSetAgricole2D(x1next,x3next)){
                                        intx3next = Indice(x3next,x3max, x3min,DX3);
                                        if (x1next < Valeur(Intx1x3frontiere[intx3next],x1max,x1min,DX1) || Intx1x3frontiere[intx3next] == DX1){
                                            b = 0;
                                            break;
                                        }
                                    }
                                    else{
                                        b = 0;
                                        break;
                                    }
                                }
                                if(b == 0){
                                    break;
                                }
                            }
                        if(b == 1){
                            break;
                        }
                    }
                    if(b == 1){
                                break;
                            }
                        }
				}
				if (b == 1){
					Intx1x3frontiere[i] = j;
					break;
				}
				else {
					change = change + 1;
				}
			}
			if (b == 0){
				Intx1x3frontiere[i] = DX1;
			}
		}

	}
}




int O3_IsinConstraintSetREstaurant2D(float x1,float x2){
	int b=0;
	if (x1>=0) b = 1;
	return b;
}


int O3_IsinAdmissibleControlSetRestaurant2D(float x1,float x2,float u3, float R, float Emin){
	int b=0;
	float x1next;
	x1next = O3_fx1Rnext(x1, x2, u3, R, Emin);
    if (x1next>=0) b = 1;

	return b;
}

//Dynamics of x1 and x2

float O3_fx1Rnext(float x1,float x2,float u3,float Rn_x3_u1,float Emin){
	float x1next;
	x1next = x1+ GainRepas(x2,u3)-CoutAchatFoodExterieur(x2,Rn_x3_u1)-fmaxf(Emin,fminf(NB_CLIENTS_MAX*x2*FPP,Rn_x3_u1)*PRICE_MTX)-FIXED_COSTSR;
	return x1next;
}

float O3_fx2next(float x2,float u3,float Rn_x3_u1){
	float x2next;
	x2next = Attractiveness_evolution(x2,u3,Rn_x3_u1);
	return x2next;
}


void O3_CalculFrontiereNoyauRestaurant2D(int Intx1frontiere[DX2],float Rmax,float Rmin,float Emin,int DR,int DE){
	float x1min,x1max,x2min,x2max,u3min,u3max,v1min,v1max,v2min,v2max,x1,x2,u3,x1next,x2next,E,R;
	int change,passage,i,j,b,ii,jj,iii,jjj,intx2next;


	x1min = fx1min();
	x1max= fx1max();
	x2min = fx2min();
	x2max= fx2max();
	u3min = fu3min();
	u3max = fu3max();

	change=1;
	passage = 1;


	for(i=0;i<DX2;i++) {
		x2= Valeur(i,x2max,x2min,DX2);
		if (O3_IsinConstraintSetREstaurant2D(X1V,x2)) Intx1frontiere[i]=X1V;
		else Intx1frontiere[i]=DX1;

	}

//Computation
	while(change>0){

		change=0;

		passage = passage + 1;
		for(i=0;i<DX2;i++){
            x2= Valeur(i,x2max,x2min,DX2);
			for(j=Intx1frontiere[i];j<DX1;j++){
				b = 0;
				x1 = Valeur(j,x1max,x1min,DX1);
				if (O3_IsinConstraintSetREstaurant2D(x1,x2)){

                        for(jj=0;jj<DU3;jj++) {
                            u3 = Valeur(jj,u3max,u3min,DU3);
                                for(jjj=0;jjj<DR;jjj++) {
                                    R = Valeur(jjj,Rmax,Rmin,DR);
                                    if (O3_IsinAdmissibleControlSetRestaurant2D(x1,x2, u3, E,Emin)){
                                b = 1;
                                x2next = O3_fx2next(x2,u3,R);
                                    x1next = O3_fx1Rnext(x1,x2,u3,R,Emin);
                                    if (O3_IsinConstraintSetREstaurant2D(x1next,x2next)){
                                        intx2next = Indice(x2next,x2max, x2min,DX2);
                                        if (x1next < Valeur(Intx1frontiere[intx2next],x1max,x1min,DX1) || Intx1frontiere[intx2next] == DX1){
                                            b = 0;
                                            break;
                                        }
                                    }
                                    else{
                                        b = 0;
                                        break;
                                    }
                                }
                                else{
                                        b = 0;
                                        break;
                                    }

                            }
                        if(b == 1){
                            break;
                        }
                    if(b == 1){
                                break;
                            }
                        }
				if (b == 1){
					Intx1frontiere[i] = j;
					break;
				}
				else {
					change = change + 1;
				}
			}
			if (b == 0){
				Intx1frontiere[i] = DX1;
			}
		}

	}
}


}


//Computation of the kernel's size
float O3_CalculSurfacenoyauAgricole2D(int Intx1x3frontiere[DX3]){
	float surface;
	int i;
	surface = 0;
	for(i=0;i<DX3;i++) {
        surface=surface+(float)(DX1)-(float)(Intx1x3frontiere[i]);
	}
	surface = surface/(DX1*DX3);
	return surface;
}

float O3_CalculSurfacenoyauRestaurant2D(int Intx2frontiere[DX2]){
	float surface;
	int i;
	surface = 0;
	for(i=0;i<DX2;i++) {
		surface=surface+(float)(DX1)-(float)(Intx2frontiere[i]);
	}
	surface = surface/(DX1*DX2);
	return surface;
}

float O3_CalculSurfacenoyauProduit(int Intx1x3frontiere[DX3], int Intx2frontiere[DX2]){
	float surface;
	int i, j, k;
	surface = 0;
	for(i=0;i<DX2;i++) {
            for(j=0;j<DX3;j++) {
                    k = Indice(Valeur(Intx2frontiere[i],X1MAX,X1MIN,DX1)+Valeur(Intx1x3frontiere[j],X1MAX,X1MIN,DX1),X1MAX,X1MIN,DX1);
		surface=surface+(float)(DX1)- (float)k;
	}}
	surface = surface/(DX1*DX2*DX3);
	return surface;
}


