# Montrieux

Code files for the computation of guaranteed viability kernels in the article “Bypassing the dimensionality curse in Viability problems: an application to a farm-restaurant interaction”

## Summary

This directory contains:
- the main.c file that computes the various viability kernels ;
- all its dependencies ;
- a dynGraph.c file returning coordinates for creating figures from the dynamics ;
- two Jupyter notebooks producing a range of figures from the C file's outputs, including Figures 3 to 5 in the article.


## Execution

`gcc main.c organisation1.c organisation2.c organisation3.c organisationU.c dynamics.c init.c utils.c affichage.c -lm -o main.exe`

