#ifndef _ORGANISATIONU_H_
#define _ORGANISATIONU_H_

#include "init.h"
#include "dynamics.h"


//************************ Computation of the viability kernel *****************************************

int IsinConstraintSet3D(float x1, float x2, float x3);


// x1 dynamics:
float fx1next(float x1,float x2, float x3, int u1,float u2, float u3,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12]);

// x2 dynamics:
float fx2next(float x2, float x3, float u1, float u2, float u3,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12]);

// x3 dynamics:
float fx3next(float x3,int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2,float x3min,float x3max);

void CalculNoyau3D(int Intx2x3frontiere[DX2][DX3]);

//Computation of the kernel's size
float CalculSurfacenoyau3D(int Intx2x3frontiere[DX2][DX3]);

// Computation if possible of a viable trajectory following the grid, starting from (x10,x20,x30) during TMAX time steps, using the kernel stored into Intx2x3frontiere[DX2][DX3]
//Returns 0 if the initial point is outside of the kernel, 1 otherwise. The coordinates of the trajectory's points are stored in traj_x1[] and the controls used are stored in traj_ui
int CalculTrajectoireViableU(float x10,float x20,float x30, int Intx2x3frontiere[DX2][DX3],float traj_x1[TMAX],float traj_x2[TMAX],float traj_x3[TMAX],int traj_u1[TMAX],float traj_u2[TMAX],float traj_u3[TMAX]);

#endif
