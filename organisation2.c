#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "organisation2.h"
#include "dynamics.h"
#include "utils.h"


//************************

// The splitting of the system can be conceived in several ways.

// Organization O2: The restaurant is only concerned by its attractivity. One control u3 is available. Agricultural activity is concerned with keeping a good soil quality: x3 >=x3QUAL; but also with keeping positive cumulative cash flows: x1>=0. 2 controls can be used, U1 and u2. 
// Negotiations between the two activities are about commitments on yearly crop production volume Rn_x3_u1 >= Rmin ; and a minimal gain from the restaurant Emin. 
// The aim is to compute the guaranteed viability kernels of both activities for various commitments, in other words various values for Rmin and Emin.


//************************

// Computation of the farmer's guaranteed viability kernel: 2 state dimensions x1 and x3, two control dimensions u1 and u2, 1 tyche Emin and two constraints plus one commitment. 


int O2_IsinConstraintSetAgricole2D(float x1, float x3){
	int b=0;
	if (x3>=X3QUAL && x1>=X1V) b = 1; // viable
	return b;
}




int O2_IsinAdmissibleControlSetAgricole2D(float x1, float x3,int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2,float Rmin){
	/// This function verifies that the couple of controls (u1, u2) meets the requirements under a state of the system (x1, x3), given the commitment made.
	int b=0;
	if (R_nS(x3,u1,Assolements,u2)>=Rmin) b = 1;
	//printf("cout %f production %f\n",Cout(u1,Assolements,u2),R_nS(x3,u1,Assolements,u2));
	return b;
}

float O2_fx1next(float x1,float u1, float u2, float E,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12]){
	float x1next;
	x1next = x1+ E-Cout(u1,Assolements,u2);
	return x1next;
}

// x3 dynamics:
float O2_fx3next(float x3,int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2,float x3min,float x3max){
	float x3next;
	x3next = BISQ_evol(x3,u1,Assolements,u2,x3min,x3max);
	return x3next;
}

void O2_CalculNoyauAgricole2D(int Intx1x3frontiere[DX3],float Rmin,float Emin,float Emax,int DE){
	float x1min,x1max,x1, x3min,x3max, u2min,u2max, x3,x1next,x3next,u2,E;
	float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12];
	int change,passage,i,j,b,ii,jj,jjj,u1,intx3next;


//Initialization of exploration bounds
	x1min = fx1min();
	x1max= fx1max();
	x3min = fx3min();
	x3max= fx3max();
	b = CreateAssolements(Assolements);
	u2min = fu2min();
	u2max = fu2max();


	change=1; // variable storing if a change was made.
	passage = 1;

//Initialization of the table storing the kernel

	for(i=0;i<DX3;i++) {
		x3= Valeur(i,x3max,x3min,DX3);
		if (O2_IsinConstraintSetAgricole2D(X1V,x3)) Intx1x3frontiere[i]=X1V;
		else Intx1x3frontiere[i]=DX1;

	}

//Computation
	while(change>0){

		change=0;

		passage = passage + 1;
		for(i=0;i<DX3;i++){
            x3= Valeur(i,x3max,x3min,DX3);

			for(j=Intx1x3frontiere[i];j<DX1;j++){

				b = 0;
				x1 = Valeur(j,x1max,x1min,DX1);

				if (O2_IsinConstraintSetAgricole2D(x1,x3)){
                    for(ii=0;ii<NB_ASSOLEMENTS;ii++) {
						u1 = ii;
					
                        for(jj=0;jj<DU2;jj++) {
                            u2 = Valeur(jj,u2max,u2min,DU2);
                       
                            if (O2_IsinAdmissibleControlSetAgricole2D(x1,x3, u1, Assolements,u2,Rmin)){
                    
                                b = 1;
                                x3next = O2_fx3next(x3,u1,Assolements,u2,x3min,x3max);
                         
                                for(jjj=0;jjj<DE;jjj++) {
                                    E = Valeur(jjj,Emax,Emin,DE);
                             
                                    x1next = O2_fx1next(x1,u1,u2,E,Assolements);
                           
                                    if (O2_IsinConstraintSetAgricole2D(x1next,x3next)){
                                        intx3next = Indice(x3next,x3max, x3min,DX3);

                                        if (x1next < Valeur(Intx1x3frontiere[intx3next],x1max,x1min,DX1) || Intx1x3frontiere[intx3next] == DX1){
                                            b = 0;
                                            break;
                                        }
                                    }
                                    else{
                                        b = 0;
                                        break;
                                    }
                                }
                                if(b == 0){
                                    break;
                                }
                            }
                        if(b == 1){
                            break;
                        }
                    }
                    if(b == 1){
                                break;
                            }
                        }
				}
				if (b == 1){
					Intx1x3frontiere[i] = j;
					break;
				}
				else {
					change = change + 1;
				}
			}
			if (b == 0){
				Intx1x3frontiere[i] = DX1;
			}
		}

	}
}




// Computation of the restaurant's viability kernel: 1 state dimension x2, 1 control u3, one tyche R and no constraint but one commitment. 

int O2_IsinConstraintSetREstaurant1D(float x2){
	int b=1;

	return b;
}

int O2_IsinAdmissibleControlSetRestaurant1D(float x2,float u3,float Rmin,float Emin){
	int b=0;
	float g;
	g = GainRepas(x2,u3)-CoutAchatFoodExterieur(x2,Rmin)-FIXED_COSTSR;
    if (g>=Emin) b = 1;

	return b;
}

// x2 Dynamics

float O2_fx2next(float x2,float u3,float Rn_x3_u1){
	float x2next;
	x2next = Attractiveness_evolution(x2,u3,Rn_x3_u1);
	return x2next;
}


void O2_CalculFrontiereNoyauRestaurant1D(int Intx2frontiere[DX2],float Rmax,float Rmin,float Emax,float Emin,int DR){
	float x2min,x2max,u3min,u3max,v1min,v1max,v2min,v2max,x1,x2,u3,x2next,E,Rn_x3_u1;
	int change,passage,i,j,b,ii,jj,iii,jjj,intx2next;


	x2min = fx2min();
	x2max= fx2max();
	u3min = fu3min();
	u3max = fu3max();


	change=1;
	passage = 1;


	for(i=0;i<DX2;i++) {
		x2= Valeur(i,x2max,x2min,DX2);
		if (O2_IsinConstraintSetREstaurant1D(x2)) Intx2frontiere[i]=1;
		else Intx2frontiere[i]=0;
	}

//Computation
	while(change>0){

		change=0;

		passage = passage + 1;
		for(i=0;i<DX2;i++){
			if (Intx2frontiere[i] == 1){
				x2= Valeur(i,x2max,x2min,DX2);
				b = 0;
	
				if (O2_IsinConstraintSetREstaurant1D(x2)){
					

					for(ii=0;ii<DU3;ii++) {
						u3 = Valeur(ii,u3max,u3min,DU3);
					
                        b = 1;
                        for(iii=0;iii<DR;iii++) {
                            Rn_x3_u1 = Valeur(iii,Rmax,Rmin,DR);
                            
                            x2next = O2_fx2next(x2,u3,Rn_x3_u1);
                            
                                if (O2_IsinAdmissibleControlSetRestaurant1D(x2,u3,Rn_x3_u1,Emin)){
								
                                    if (O2_IsinConstraintSetREstaurant1D(x2next)){
                                        intx2next = Indice(x2next,x2max, x2min,DX2);
								
                                        if (Intx2frontiere[intx2next]==0){
                                            b = 0; // viable
                                            break;
                                        }
                                    }
                                    else{
                                        b = 0;
                                        break;
                                    }
                                }
                                else{
                                        b = 0;
                                        break;
                                    }
                        }
                        if (b ==1){
                            break;
                        }
					}
					if (b == 0){
						Intx2frontiere[i] = 0;
						change = change + 1;
					}
                    }
				else 	Intx2frontiere[i] = 0;

			}
		}
	}


}


//Computation of the kernel's size
float O2_CalculSurfacenoyauAgricole2D(int Intx1x3frontiere[DX3]){
	float surface;
	int i;
	surface = 0;
	for(i=0;i<DX3;i++) {
        surface=surface+(float)(DX1)-(float)(Intx1x3frontiere[i]);
	}
	surface = surface/(DX1*DX3);
	return surface;
}

float O2_CalculSurfacenoyauRestaurant1D(int Intx2frontiere[DX2]){
	float surface;
	int i;
	surface = 0;
	for(i=0;i<DX2;i++) {
		if (Intx2frontiere[i] == 1) surface=surface+1;
	}
	surface = surface/(DX2);
	return surface;
}

float O2_CalculSurfacenoyauProduit(int Intx1x3frontiere[DX3], int Intx2frontiere[DX2]){
	float surface;
	int i, j;
	surface = 0;
	for(i=0;i<DX2;i++) {
        if (Intx2frontiere[i] == 1){
            for(j=0;j<DX3;j++) {

                surface=surface+(float)(DX1)- (float)(Intx1x3frontiere[j]);
	}}}
	surface = surface/(DX1*DX2*DX3);
	return surface;
}


