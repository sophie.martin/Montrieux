#include <stdio.h>
#include "organisationU.h"
#include "dynamics.h"
#include "utils.h"
#include "organisationU.h"


//************************ Computation of the viability kernel

int IsinConstraintSet3D(float x1, float x2, float x3){
    // Returns 1 if viable, 0 otherwise
	int b=1;
	if (x1<=0) b = 0;
	else if (x3<X3QUAL) b = 0;
	return b;
}


// x1 dynamics:
float fx1next(float x1,float x2, float x3, int u1,float u2, float u3,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12]){
	float x1next;
	float Rn_x3_u1 = R_nS(x3, u1,Assolements,u2);
	x1next = x1+ GainRepas(x2,u3)-CoutAchatFoodExterieur(x2,Rn_x3_u1)-FIXED_COSTSR-Cout(u1, Assolements, u2);
	return x1next;
}


// x2 dynamics:
float fx2next(float x2, float x3, float u1, float u2, float u3,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12]){
	float x2next;
	float Rn_x3_u1;
	Rn_x3_u1 = R_nS(x3, u1,Assolements,u2);
	x2next = Attractiveness_evolution(x2,u3,Rn_x3_u1);
	return x2next;
}

// x3 dynamics:
float fx3next(float x3,int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2,float x3min,float x3max){
	float x3next;
	x3next = BISQ_evol(x3,u1,Assolements,u2,x3min,x3max);
	return x3next;
}

void CalculNoyau3D(int Intx2x3frontiere[DX2][DX3]){
	float x1,x2, x3,x1min,x1max,x2min,x2max, x3min,x3max,x1next,x2next,x3next, u2min,u2max,u3min,u3max,u3,u2;
	float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12];
	int change=1,passage=1,i1,i2,i3,j1,j2,j3,b,iii,jjj,u1,intx2next,intx3next;



       x1min = fx1min();
	x1max= fx1max();
	x2min = fx2min();
	x2max= fx2max();
	u3min = fu3min();
	u3max = fu3max();
	x3min = fx3min();
	x3max= fx3max();
	CreateAssolements(Assolements);
	u2min = fu2min();
	u2max = fu2max();




    for(i2=0;i2<DX2;i2++) {
        for(i3=0;i3<DX3;i3++) {
            for (i1=0;i1<DX1;i1++){
                x1 = Valeur(i1,x1max,x1min,DX1);
                x2 = Valeur(i2,x2max,x2min,DX2);
                x3= Valeur(i3,x3max,x3min,DX3);
                if (IsinConstraintSet3D(x1,x2,x3)) {
                        Intx2x3frontiere[i2][i3]=i1;
                        break;
                }
                else if (i1==DX1-1) Intx2x3frontiere[i2][i3]=DX1;
    }}}


//Kernel's computation
		while(change>0){
            change=0;
            printf("passage %d\n",passage);
            passage = passage + 1;
            for (i3=0;i3<DX3;i3++){
                x3 = Valeur(i3,x3max,x3min,DX3);
                printf("t %d x3  %d \n",passage,i3);
                for(i2=0;i2<DX2;i2++){
                    x2= Valeur(i2,x2max,x2min,DX2);
                    for(i1=Intx2x3frontiere[i2][i3];i1<DX1;i1++){
                        b = 0;
                        x1 = Valeur(i1,x1max,x1min,DX1);
                        if (IsinConstraintSet3D(x1,x2,x3)){
                            for(j1=0;j1<NB_ASSOLEMENTS;j1++) {
                                u1 = j1;
                                for(j2=0;j2<DU2;j2++) {
                                    u2 = Valeur(j2,u2max,u2min,DU2);
                                    for(j3=0;j3<DU3;j3++) {
                                        u3 = Valeur(j3,u3max,u3min,DU3);
                                        x1next = fx1next(x1,x2,x3,u1,u2,u3,Assolements);
                                        x2next = fx2next(x2,x3,u1,u2,u3,Assolements);
                                        x3next = fx3next(x3,u1,Assolements,u2,x3min,x3max);
                                        if (IsinConstraintSet3D(x1next,x2next,x3next)){
                                            intx2next = Indice(x2next,x2max, x2min,DX2);
                                            intx3next = Indice(x3next, x3max, x3min, DX3);
                                            if (x1next > Valeur(Intx2x3frontiere[intx2next][intx3next],x1max,x1min,DX1) && Intx2x3frontiere[intx2next][intx3next] < DX1){
                                                b = 1; // viable
                                                break;
                                            }
                                        }
                                    }
                                    if(b == 1){
                                        break;
                                    }
                                }
                                if(b == 1){
                                    break;
                                }
                            }
                        }
                        if (b == 1){
                            Intx2x3frontiere[i2][i3] = i1;
                            break;
                        }
                        else {
                            change = change + 1;
                        }
                    }
                    if (b == 0){
                        Intx2x3frontiere[i2][i3] = DX1;
                    }
                }
            }

        }

}

//Computation of the kernel's size
float CalculSurfacenoyau3D(int Intx2x3frontiere[DX2][DX3]){
	float surface;
	int i,j;
	surface = 0;
	for(i=0;i<DX2;i++) {
        for(j=0;j<DX3;j++) {
            surface=surface+(float)(DX1)-(float)(Intx2x3frontiere[i][j]);
        }
	}
	surface = surface/(DX1*DX2*DX3);
	return surface;
}

// Computation if possible of a viable trajectory following the grid, starting from (x10,x20,x30) during TMAX time steps, using the kernel stored into Intx2x3frontiere[DX2][DX3]
//Returns 0 if the initial point is outside of the kernel, 1 otherwise. The coordinates of the trajectory's points are stored in traj_x1[] and the controls used are stored in traj_ui
int CalculTrajectoireViableU(float x10,float x20,float x30, int Intx2x3frontiere[DX2][DX3],float traj_x1[TMAX],float traj_x2[TMAX],float traj_x3[TMAX],int traj_u1[TMAX],float traj_u2[TMAX],float traj_u3[TMAX]){
	int b=0;
	int t=0;
	int j1,j2,j3,u1;
	float u2,u3,x1current,x2current,x3current,x1,x2,x3,x1next,x2next,x3next;
	float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12];
	float x1min,x1max,x2min,x2max,x3min,x3max,u2min,u2max,u3min,u3max;

	//printf("x10 %f x20 %f x30 %f", x10, x20, x30);

	if (IsinConstraintSet3D(x10,x20,x30)){
		x1min = fx1min();
		x1max= fx1max();
		x2min = fx2min();
		x2max= fx2max();
		u3min = fu3min();
		u3max = fu3max();
		x3min = fx3min();
		x3max= fx3max();
		CreateAssolements(Assolements);
		u2min = fu2min();
		u2max = fu2max();
		x1current = x10;
		x2current = x20;
		x3current = x30;
		while (t<TMAX){
			//printf("xcurrent %f %f %f\n",x1current,x2current,x3current);
			printf(" t %d\n",t);
			b = 0;
			x1 = Valeur(Indice(x1current,x1max, x1min,DX1),x1max, x1min,DX1);
			x2 = Valeur(Indice(x2current,x2max, x2min,DX2),x2max, x2min,DX2);
			x3 = Valeur(Indice(x3current,x3max, x3min,DX3),x3max, x3min,DX3);
			
            for(j3=0;j3<DU3;j3++) {
                u3 = Valeur(j3,u3max,u3min,DU3);
                for(j2=0;j2<DU2;j2++) {
                    u2 = Valeur(j2,u2max,u2min,DU2);
                    for(j1=0;j1<NB_ASSOLEMENTS;j1++) {
                        u1 = j1;
                        x1next = fx1next(x1,x2,x3,u1,u2,u3,Assolements);
                        x2next = fx2next(x2,x3,u1,u2,u3,Assolements);
                        x3next = fx3next(x3,u1,Assolements,u2,x3min,x3max);

                        if ((x1next > Valeur(Intx2x3frontiere[Indice(x2next,x2max, x2min,DX2)][Indice(x3next,x3max, x3min,DX3)],x1max,x1min,DX1)) && (Intx2x3frontiere[Indice(x2next,x2max, x2min,DX2)][Indice(x3next,x3max, x3min,DX3)] < DX1)) {
                            b = 1; // viable

                            break;
                            }

                        }
                        if (b ==1) break;
                    }
                    if (b ==1) break;

			}
			if (b==0) {printf("problem");getchar();}
			traj_x1[t] = x1;
			traj_x2[t] = x2;
			traj_x3[t] = x3;
			traj_u1[t] = u1;
			traj_u2[t] = u2;
			traj_u3[t] = u3;
			x1current = x1next;
			x2current = x2next;
			x3current = x3next;
			t =t+1;
		}
	}
	else{
        printf("Starting point is not viable.");
	}
	return b;
}

