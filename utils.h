#ifndef _UTILS_H_
#define _UTILS_H_

// Function returning the index associated to a real value, given the bounds and precision D

int Indice(float x,float xmax, float xmin,int Dx);


// Function returning the value associated to the index.

float Valeur(int ix,float xmax, float xmin,int Dx);

#endif
