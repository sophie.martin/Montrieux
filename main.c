#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include "organisation1.h"
#include "organisation2.h"
#include "organisation3.h"
#include "organisationU.h"
#include "utils.h"
#include "dynamics.h"
#include "affichage.h"
#include "init.h"





int main(int argc, char *argv[]){
	int b,i,j;
	float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12];
//Checking the table of crop rotations
/*	b = CreateAssolements(Assolements);
	for (i=0;i<1;i++){
		for (j=0;j<6*NB_CROPS+12;j++){
			printf("%f, ",Assolements[i][j]);
		}
		printf("\n");
	}
*/

/*
	int Intx3[DX3],Intx2frontiere[DX2];
	int Intx2x3frontiere[DX2][DX3];
	FILE *f;
	f = fopen("test_print_python.txt", "w");

	for(j=0;j<DX3;j++) {
		Intx3[j]=j%2;
	}
	for(i=0;i<DX2;i++) {
		Intx2frontiere[i]=i;
	}
	for(i=0;i<DX2;i++) {
		for(j=0;j<DX3;j++) {
			Intx2x3frontiere[i][j]=j;
		}
	}

	Affichage_pour_affichage_python(f,Intx3,Intx2frontiere,Intx2x3frontiere);
	fclose(f);
*/

////Printing the kernels for O1, O2, O3 et U
//	int Intx3[DX3],Intx2frontiere[DX2];
//	int Intx2[DX2],Intx3frontiere[DX3];
//	int O3Intx2[DX2],O3Intx3frontiere[DX3];
//	int Intx2x3frontiere[DX2][DX3], traj_u1[TMAX];
//	float traj_x1[TMAX],traj_x2[TMAX], traj_x3[TMAX], traj_u2[TMAX], traj_u3[TMAX];
//	clock_t start, end;
//	double run_time;
//
//	int DR = 3;
//	int DE = 4;
//	float RMAX = production_maximale();
//	float EMAX = cout_maximal();
//
//	float Rmin = 20000;
//	float Emax = 39500;
//	float Emin = 39500;
//
//
//	FILE *f;
//	f = fopen("test_print_python.txt", "w");
//	printf("O1_Agriculture\n");
//	start = clock();
//	O1_CalculNoyauAgricole1D(Intx3,Rmin,Emax);
//	printf("O1_Restaurant\n");
//	O1_CalculFrontiereNoyauRestaurant2D(Intx2frontiere,RMAX,Rmin,Emax,0,DR,DE);
//	end = clock();
//	run_time = ((double)(end-start))/CLOCKS_PER_SEC;
//	printf("Execution time O1 : %f \n",run_time);
//
//	printf("O2_Agriculture\n");
//	start = clock();
//	O2_CalculNoyauAgricole2D(Intx3frontiere,Rmin,Emin,EMAX,DE);
//	printf("O2_Restaurant\n");
//	O2_CalculFrontiereNoyauRestaurant1D(Intx2,RMAX,Rmin,EMAX, Emin,DR);
//	end = clock();
//	run_time = ((double)(end-start))/CLOCKS_PER_SEC;
//	printf("Execution time O2 : %f \n",run_time);
//
//	printf("O3_Agriculture\n");
//	start = clock();
//	O3_CalculNoyauAgricole2D(O3Intx3frontiere,Rmin,Emin,EMAX,DE);
//	printf("O3_Restaurant\n");
//	O3_CalculFrontiereNoyauRestaurant2D(O3Intx2,RMAX,Rmin, Emin,DR,DE);
//	end = clock();
//	run_time = ((double)(end-start))/CLOCKS_PER_SEC;
//	printf("Execution time O3 : %f \n",run_time);
//
//	printf("Unique\n");
//	FILE *f_ker;
//    start = clock();
//
//	CalculNoyau3D(Intx2x3frontiere);
//	f_ker = fopen("kernel3D.dat", "wb");
//	SaveKernel(f_ker, Intx2x3frontiere);
//	fclose(f_ker);
//
////	f_ker = fopen("kernel3D.dat", "rb");
////	LoadKernel(f_ker, Intx2x3frontiere);
////    fclose(f_ker);
//
//    end = clock();
//	run_time = ((double)(end-start))/CLOCKS_PER_SEC;
//	printf("Execution time U : %f \n",run_time);
//
//    Affichage_pour_affichage_python(f,Intx3,Intx2frontiere,Intx2, Intx3frontiere,O3Intx3frontiere,O3Intx2, Intx2x3frontiere);

//
//	printf("Trajectories\n");
//
//	fprintf(f,"\nTrajectory unified kernel:\n");
//	CalculTrajectoireViableU(10000.0,0.651,1.0,Intx2x3frontiere,traj_x1,traj_x2,traj_x3,traj_u1,traj_u2,traj_u3);
//    Affichage_Traj(f,traj_x1,traj_x2,traj_x3,traj_u1,traj_u2,traj_u3);
//
//    fprintf(f,"\nTrajectory cross-product:\n");
//    O1_CalculTrajectoireViable(15000.0,0.651,1.0,Intx3,Intx2frontiere,traj_x1,traj_x2,traj_x3,traj_u1,traj_u2,traj_u3);
//    Affichage_Traj(f,traj_x1,traj_x2,traj_x3,traj_u1,traj_u2,traj_u3);
//
//    fprintf(f,"\nRandom trajectory cross-product kernel:\n");
//    O1_CalculTrajectoireViableAlea(15000.0,0.651,1.0,Intx3,Intx2frontiere,traj_x1,traj_x2,traj_x3,traj_u1,traj_u2,traj_u3);
//    Affichage_Traj(f,traj_x1,traj_x2,traj_x3,traj_u1,traj_u2,traj_u3);
//
//	fclose(f);


// Kernels organization 1
//
//	int Intx3frontiere[DX3],Intx1frontiere[DX2];
//    float surfaces_Agricole1D[O1_DR][O1_DE],surfaces_Restaurant2D[O1_DR][O1_DE], surfaces_produit[O1_DR][O1_DE], comp_times[O1_DR][O1_DE];
//	float Rmin,Emax;
//	clock_t start, end;
//	double run_time;
//
//	int DR = 3;
//	int DE = 4;
//	float RMAX = production_maximale();
//	float EMAX = cout_maximal();
//	printf("RMAX %f ",RMAX);
//	printf("EMAX %f ",EMAX)/2;
//
//	for (i=0;i<O1_DR;i++){
//		Rmin = Valeur(i,RMAX,0.0,O1_DR);
//		printf("Rmin %f \n",Rmin);
//		for (j=0;j<O1_DE;j++){
//			Emax = Valeur(j,EMAX,0.0,O1_DE);
//			//printf("Emax %f ",Emax);
//			//printf("Kernelx3 ");
//			start = clock();
//			O1_CalculNoyauAgricole1D(Intx3frontiere,Rmin,Emax);
//			//printf("Kernelx1x2 ");
//			O1_CalculFrontiereNoyauRestaurant2D(Intx1frontiere,RMAX,Rmin,Emax,0,DR,DE);
//			end = clock();
//			surfaces_Agricole1D[i][j] = O1_CalculSurfacenoyauAgricole1D(Intx3frontiere);
//			surfaces_Restaurant2D[i][j] = O1_CalculSurfacenoyauRestaurant2D(Intx1frontiere);
//			surfaces_produit[i][j] = O1_CalculSurfacenoyauProduit(Intx3frontiere,Intx1frontiere);
//			run_time = ((double)(end-start))/CLOCKS_PER_SEC;
//			comp_times[i][j] = run_time;
//		}
//	}
//	for (i=0;i<O1_DR;i++){
//		if (i==0) printf("[");
//		else printf(",");
//		Rmin = Valeur(i,RMAX,0.0,O1_DR);
//		printf("%f",Rmin);
//		if (i==O1_DR-1) printf("]");
//	}
//	printf("\n");
//	for (i=0;i<O1_DE;i++){
//		if (i==0) printf("[");
//		else printf(",");
//		Emax = Valeur(i,EMAX,0.0,O1_DE);
//		printf("%f",Emax);
//		if (i==O1_DE-1) printf("]");
//	}
//	printf("\n RESTAURANT\n");
//	for (i=0;i<O1_DE;i++){
//		if (i==0) printf("[");
//		else printf(",");
//		for (j=0;j<O1_DR;j++){
//			if (j==0) printf("[");
//			else printf(",");
//			printf("%f",surfaces_Restaurant2D[j][i]);
//			if (j==O1_DR-1) printf("]");
//		}
//		if (i==O1_DE-1) printf("]");
//	}
//
//	printf("\n AGRICULTURE\n");
//	for (i=0;i<O1_DE;i++){
//		if (i==0) printf("[");
//		else printf(",");
//		for (j=0;j<O1_DR;j++){
//			if (j==0) printf("[");
//			else printf(",");
//			printf("%f",surfaces_Agricole1D[j][i]);
//			if (j==O1_DR-1) printf("]");
//		}
//		if (i==O1_DE-1) printf("]");
//	}
//
//
//	FILE* sauv = NULL ;
//	sauv = fopen("Results.txt", "a");
//	FILE* sauvT = fopen("Exec_timesO1.txt", "w");
//	if (sauv != NULL){
//        InscriptionParametres(sauv);
//        O1_InscriptionResultats(sauv, surfaces_Agricole1D, surfaces_Restaurant2D, surfaces_produit);
//        fclose(sauv);
//        SaveTimes(sauvT,comp_times);
//        fclose(sauvT);
//	}
//	else {
//        printf("WARNING : File saving failed !");
//	}




// Kernel unified organization

//	float surface_3D;
//	int Intx2x3frontiere[DX2][DX3];
//	float traj_x1[TMAX],traj_x2[TMAX],traj_x3[TMAX],traj_u1[TMAX],traj_u2[TMAX],traj_u3[TMAX];
//
//    CalculNoyau3D(Intx2x3frontiere);
//
//    surface_3D = CalculSurfacenoyau3D(Intx2x3frontiere);
//    printf("Surface : %f /n",surface_3D);
//
//    for (i=0;i<DX2;i++){
//        if (i==0) printf("[");
//        for (j=0;j<DX3;j++){
//            if (j==0) printf("[");
//            else printf(",");
//            printf("%d", Intx2x3frontiere[i][j]);
//            if (j==DX3-1) {
//                printf("]");
//                if (i<DX2-1) printf(",");
//            }
//        }
//        if (i==DX2-1) printf("]");
//
//   }
//
//
//	FILE* sauv = NULL ;
//	sauv = fopen("ResultsU.txt", "a");
//	if (sauv != NULL){
//        InscriptionParametres(sauv);
//        InscriptionResultatsU(sauv, Intx2x3frontiere);
//       fclose(sauv);
//	}
//	else {
//        printf("WARNING : File saving failed !");
//	}
//
//
//
//   b = CalculTrajectoireViableU(0.0,0.8,1.0,Intx2x3frontiere,traj_x1,traj_x2,traj_x3,traj_u1,traj_u2,traj_u3);
//
//   sauv = fopen("Traj.txt", "a");
//	if (sauv != NULL){
//        Affichage_Traj(sauv,traj_x1,traj_x2,traj_x3,traj_u1,traj_u2,traj_u3);
//       fclose(sauv);
//	}
//	else {
//        printf("WARNING : File saving failed !");
//	}

//    printf("traj_x1 = [");
//    for (i=0;i<TMAX;i++){
//    	printf("%f",traj_x1[i]);
//    	if (i<TMAX-1) printf(",");
//    }
//    printf("]\n");
//    printf("traj_x2 = [");
//    for (i=0;i<TMAX;i++){
//    	printf("%f",traj_x2[i]);
//    	if (i<TMAX-1) printf(",");
//    }
//    printf("]\n");
//    printf("traj_x3 = [");
//    for (i=0;i<TMAX;i++){
//    	printf("%f",traj_x3[i]);
//    	if (i<TMAX-1) printf(",");
//    }
//    printf("]\n");
//    printf("traj_u1 = [");
//    for (i=0;i<TMAX;i++){
//    	printf("%f",traj_u1[i]);
//    	if (i<TMAX-1) printf(",");
//    }
//    printf("]\n");
//    printf("traj_u2 = [");
//    for (i=0;i<TMAX;i++){
//    	printf("%f",traj_u2[i]);
//    	if (i<TMAX-1) printf(",");
//    }
//    printf("]\n");
//    printf("traj_u3 = [");
//    for (i=0;i<TMAX;i++){
//    	printf("%f",traj_u3[i]);
//    	if (i<TMAX-1) printf(",");
//    }
//    printf("]\n");


// Multiple random trajectories
//	int Intx3[DX3],Intx2frontiere[DX2];
//	int Intx2x3frontiere[DX2][DX3], traj_u1[TMAX];
//	float traj_x1[TMAX],traj_x2[TMAX], traj_x3[TMAX], traj_u2[TMAX], traj_u3[TMAX];
//	int DR = 3;
//	int DE = 4;
//	float RMAX = production_maximale();
//	int NB_Traj = 10;
//
//
//	FILE *f;
//	f = fopen("traj_rand.txt", "w");
//
//    fprintf(f, "T = %d\n", TMAX);
//
//	O1_CalculNoyauAgricole1D(Intx3,12000,39500);
//	O1_CalculFrontiereNoyauRestaurant2D(Intx2frontiere,RMAX,12000,39500,0,DR,DE);
//
//
//
//    for (i=0;i<NB_Traj;i++){
//    O1_CalculTrajectoireViableAlea(11000.0,0.7,1.0,Intx3,Intx2frontiere,traj_x1,traj_x2,traj_x3,traj_u1,traj_u2,traj_u3);
//    Affichage_Traj_i(f,traj_x1,traj_x2,traj_x3,traj_u1,traj_u2,traj_u3,i);
//    getchar(); 
//    }
//
//    fclose(f);



// Kernels organization 2
//
//	int Intx1x3frontiere[DX3],Intx2frontiere[DX2];
//	float surfaces_Agricole2D[O2_DR][O2_DE],surfaces_Restaurant1D[O2_DR][O2_DE],surfaces_produit[O2_DR][O2_DE], comp_times[O2_DR][O2_DE];
//	float Rmin,Emin;
//
//    clock_t start, end;
//	double run_time;
//
//	int DR = 3;
//	int DE = 4;
//	float RMAX = production_maximale();
//	float EMAX = cout_maximal();
//	printf("RMAX %f ",RMAX);
//	printf("EMAX %f ",EMAX);
//	for (i=0;i<O2_DR;i++){
//		Rmin = Valeur(i,RMAX,0.0,O2_DR);
//		printf("\nRmin %f \n",Rmin);
//		for (j=0;j<O2_DE;j++){
//			Emin = Valeur(j,EMAX,0.0,O2_DE);
//			//printf("Emin %f ",Emin);
//			//printf("Kernelx3 ");
//			start = clock();
//			O2_CalculNoyauAgricole2D(Intx1x3frontiere,Rmin,Emin,EMAX,DE);
//			//printf("Kernelx1x2 ");
//			O2_CalculFrontiereNoyauRestaurant1D(Intx2frontiere,RMAX,Rmin,EMAX,Emin,DR);
//			end = clock();
//			surfaces_Agricole2D[i][j] = O2_CalculSurfacenoyauAgricole2D(Intx1x3frontiere);
//			surfaces_Restaurant1D[i][j] = O2_CalculSurfacenoyauRestaurant1D(Intx2frontiere);
//			surfaces_produit[i][j] = O2_CalculSurfacenoyauProduit(Intx1x3frontiere,Intx2frontiere);
//			run_time = ((double)(end-start))/CLOCKS_PER_SEC;
//			comp_times[i][j] = run_time;
//		}
//	}
//	for (i=0;i<O2_DR;i++){
//		if (i==0) printf("[");
//		else printf(",");
//		Rmin = Valeur(i,RMAX,0.0,O2_DR);
//		printf("%f",Rmin);
//		if (i==O2_DR-1) printf("]");
//	}
//	printf("\n");
//	for (i=0;i<O2_DE;i++){
//		if (i==0) printf("[");
//		else printf(",");
//		Emin = Valeur(i,EMAX,0.0,O2_DE);
//		printf("%f",Emin);
//		if (i==O2_DE-1) printf("]");
//	}
//	printf("\n RESTAURANT\n");
//	for (i=0;i<O2_DE;i++){
//		if (i==0) printf("[");
//		else printf(",");
//		for (j=0;j<O2_DR;j++){
//			if (j==0) printf("[");
//			else printf(",");
//			printf("%f",surfaces_Restaurant1D[j][i]);
//			if (j==O2_DR-1) printf("]");
//		}
//		if (i==O2_DE-1) printf("]");
//	}
//
//	printf("\n AGRICULTURE\n");
//	for (i=0;i<O2_DE;i++){
//		if (i==0) printf("[");
//		else printf(",");
//		for (j=0;j<O2_DR;j++){
//			if (j==0) printf("[");
//			else printf(",");
//			printf("%f",surfaces_Agricole2D[j][i]);
//			if (j==O2_DR-1) printf("]");
//		}
//		if (i==O2_DE-1) printf("]");
//	}
//
//
//	FILE* sauv = NULL ;
//	sauv = fopen("ResultsO2.txt", "a");
//	FILE* sauvT = fopen("Exec_timesO2.txt", "w");
//	if (sauv != NULL){
//        InscriptionParametres(sauv);
//        O2_InscriptionResultats(sauv, surfaces_Agricole2D, surfaces_Restaurant1D, surfaces_produit);
//        fclose(sauv);
//        SaveTimes(sauvT,comp_times);
//        fclose(sauvT);
//	}
//	else {
//        printf("WARNING : File saving failed !");
//	}



// Kernels organization 3

	int Intx1x3frontiere[DX3],Intx2frontiere[DX2];
	float surfaces_Agricole2D[O3_DR][O3_DE],surfaces_Restaurant2D[O3_DR][O3_DE],surfaces_produit[O3_DR][O3_DE], comp_times[O3_DR][O3_DE];
	float Rmin,Emin;

    clock_t start, end;
	double run_time;

	int DR = 3;
	int DE = 4;
	float RMAX = production_maximale();
	float EMAX = cout_maximal();
	printf("RMAX %f ",RMAX);
	printf("EMAX %f ",EMAX);
	for (i=0;i<O3_DR;i++){
		Rmin = Valeur(i,RMAX,0.0,O3_DR);
		printf("\nRmin %f \n",Rmin);
		for (j=0;j<O3_DE;j++){
			Emin = Valeur(j,EMAX,0.0,O3_DE);
			//printf("Emin %f ",Emin);
			//printf("Kernelx3 ");
            start = clock();
			O3_CalculNoyauAgricole2D(Intx1x3frontiere,Rmin,Emin,EMAX,DE);
			//printf("Kernelx1x2 ");
			O3_CalculFrontiereNoyauRestaurant2D(Intx2frontiere,RMAX,Rmin,Emin,DR, DE);
            end = clock();
			surfaces_Agricole2D[i][j] = O3_CalculSurfacenoyauAgricole2D(Intx1x3frontiere);
			surfaces_Restaurant2D[i][j] = O3_CalculSurfacenoyauRestaurant2D(Intx2frontiere);
            surfaces_produit[i][j] = O3_CalculSurfacenoyauProduit(Intx1x3frontiere, Intx2frontiere);
            run_time = ((double)(end-start))/CLOCKS_PER_SEC;
			comp_times[i][j] = run_time;



		}
	}
	for (i=0;i<O3_DR;i++){
		if (i==0) printf("[");
		else printf(",");
		Rmin = Valeur(i,RMAX,0.0,O3_DR);
		printf("%f",Rmin);
		if (i==O3_DR-1) printf("]");
	}
	printf("\n");
	for (i=0;i<O3_DE;i++){
		if (i==0) printf("[");
		else printf(",");
		Emin = Valeur(i,EMAX,0.0,O3_DE);
		printf("%f",Emin);
		if (i==O3_DE-1) printf("]");
	}
	printf("\n RESTAURANT\n");
	for (i=0;i<O3_DE;i++){
		if (i==0) printf("[");
		else printf(",");
		for (j=0;j<O3_DR;j++){
			if (j==0) printf("[");
			else printf(",");
			printf("%f",surfaces_Restaurant2D[j][i]);
			if (j==O3_DR-1) printf("]");
		}
		if (i==O3_DE-1) printf("]");
	}

	printf("\n AGRICULTURE\n");
	for (i=0;i<O3_DE;i++){
		if (i==0) printf("[");
		else printf(",");
		for (j=0;j<O3_DR;j++){
			if (j==0) printf("[");
			else printf(",");
			printf("%f",surfaces_Agricole2D[j][i]);
			if (j==O3_DR-1) printf("]");
		}
		if (i==O3_DE-1) printf("]");
	}


	FILE* sauv = NULL ;
	sauv = fopen("ResultsO3.txt", "a");
	FILE* sauvT = fopen("Exec_timesO3.txt", "w");


	if (sauv != NULL){
        InscriptionParametres(sauv);
        O3_InscriptionResultats(sauv, surfaces_Agricole2D, surfaces_Restaurant2D, surfaces_produit);
        fclose(sauv);
        SaveTimes(sauvT,comp_times);
        fclose(sauvT);
	}
	else {
        printf("WARNING : File saving failed !");
	}


   return 0;
}

