#ifndef merdeINIT_H_
#define merdeINIT_H_





//Paramètres boites
#define X1MAX 100000.0 //
#define X1MIN 0.0
#define X2MAX 1.0
#define X2MIN 0.0
#define X3MAX 1.0
#define X3MIN 0.0
#define NB_CROPS 5 // crops planted each year
#define NB_POSSIBLE_CROPS 9 // crops available
#define NB_ASSOLEMENTS 126 //9*8*7*6*5/5/4/3/2  // Crops vs possible
#define U2MAX 2.0
#define U2MIN 0.05
#define U3MAX 15.0
#define U3MIN 2.0

//Pour les grilles
#define DX1 41
#define DX2 31
#define DX3 51
#define DU2 21
#define DU3 21




//The following functions compute the borders of the considered set for the 3 state variables: x1, x2, x3

float fx1max();
float fx1min();
float fx2max();
float fx2min();
float fx3max();
float fx3min();

//This function lists the informations required for the crop portfolios
int CreateAssolements(float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12]);

//The following functions compute the borders of the considered set for 2 control variables: u2, u3

float fu2max();
float fu2min();
float fu3max();
float fu3min();

#endif
