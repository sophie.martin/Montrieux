#ifndef _AFFICHAGE_H_
#define _AFFICHAGE_H_

#include "organisation1.h"
#include "organisation2.h"
#include "organisation3.h"

#define PREFIXEFICHIER "GuarViab_Montrieux"
#define VERSION "2.2"

void InscriptionParametres(FILE *f);

void O1_InscriptionResultats(FILE *f, float surfaces_Agricole1D[O1_DR][O1_DE], float surfaces_Restaurant2D[O1_DR][O1_DE], float surfaces_produit[O1_DR][O1_DE]);

void O2_InscriptionResultats(FILE *f, float surfaces_Agricole2D[O2_DR][O2_DE], float surfaces_Restaurant1D[O2_DR][O2_DE], float surfaces_produit[O2_DR][O2_DE]);

void O3_InscriptionResultats(FILE *f, float surfaces_Agricole2D[O3_DR][O3_DE], float surfaces_Restaurant2D[O3_DR][O3_DE], float surfaces_produit[O3_DR][O3_DE]);

void InscriptionResultatsU(FILE *f, int Values[DX2][DX3]);

void Affichage_vecteur(FILE *f,float xmax,float xmin,int dx);

void Affichage_Noyau_1D_x3(FILE *f,int Values[DX3]);

void Affichage_Frontiere_Noyau_2D_x2x1(FILE *f,int Values[DX2]);

void Affichage_Noyau_1D_x2(FILE *f,int Values[DX2]);

void Affichage_Frontiere_Noyau_2D_x3x1(FILE *f,int Values[DX3]);

void Affichage_Frontiere_Noyau_3D_x2x3x1(FILE *f,int Values[DX2][DX3]);

void Affichage_Traj(FILE *f, float traj_x1[TMAX],float traj_x2[TMAX],float traj_x3[TMAX],int traj_u1[TMAX],float traj_u2[TMAX],float traj_u3[TMAX]);
void Affichage_Traj_i(FILE *f, float traj_x1[TMAX],float traj_x2[TMAX],float traj_x3[TMAX],int traj_u1[TMAX],float traj_u2[TMAX],float traj_u3[TMAX],int n);

//Sortie destinée à être copiée_collée dans le otebook python
void Affichage_pour_affichage_python(FILE *f,int Values_O1_x3[DX3],int Values_O1_x2x1[DX2],int Values_O2_x2[DX2],int Values_O2_x3x1[DX3],int Values_O3_x3x1[DX3],int Values_O3_x2x1[DX2],int ValuesU[DX2][DX3]);

// Functions to save and load 3D kernel
void SaveKernel(FILE *f,int ValuesU[DX2][DX3]);

void LoadKernel(FILE *f,int ValuesU[DX2][DX3]);

void SaveTimes(FILE *f, float comp_times[O1_DR][O1_DE]);

#endif
