#include <math.h>
#include <stdio.h>
#include "dynamics.h"
#include "init.h"
#include "utils.h"
#include "organisation1.h"



int CalculRdts(float R[DU2][NB_ASSOLEMENTS], float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12]){
    int j1, j2, u1;
    float u2, u2min, u2max;
    float x3 = 1.0;

    u2min = fu2min();
	u2max = fu2max();

    for (j1=0;j1<NB_ASSOLEMENTS;j1++) {
        u1 = j1;
            for (j2=0;j2<DU2;j2++) {
                    u2 = Valeur(j2,u2max,u2min,DU2);
                R[j2][j1] = R_nS(x3,u1,Assolements,u2);
            }
    }

    return 0;
}



int CalculEvolAttract_u1u3(float traj[DU3][NB_ASSOLEMENTS], float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12]){
	int j1, j3, u1;
	float u3, u3min, u3max, R, A ;
	float x3 = 1.0;
	float u2 = U2MIN;

	u3min = fu3min();
	u3max = fu3max();

	for(j3=0;j3<DU3;j3++) {
        u3 = Valeur(j3,u3max,u3min,DU3);
        for(j1=0;j1<NB_ASSOLEMENTS;j1++) {
            u1 = j1;
            R = R_nS(x3,u1,Assolements,u2);

            A = Attractiveness_evolution(1,u3,R);
            //printf("%f",A);
            traj[j3][j1] = A ;
        }
	}

    return 0;
}

int CalculEvolAttract_u2u3(float traj[DU3][DU2], float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12]){
	int j1, j3;
	float u2, u2min, u2max, u3, u3min, u3max, R, A ;
	float x3 = 0.3;
	int u1 = 0;

	u2min = fu2min();
	u2max = fu2max();
	u3min = fu3min();
	u3max = fu3max();

	for(j3=0;j3<DU3;j3++) {
        u3 = Valeur(j3,u3max,u3min,DU3);
        for(j1=0;j1<DU2;j1++) {
            u2 = Valeur(j1,u2max,u2min,DU2);
            R = R_nS(x3,u1,Assolements,u2);

            A = Attractiveness_evolution(1,u3,R);
            //printf("%f",A);
            traj[j3][j1] = A ;
        }
	}

    return 0;
}


int CalculEvolAttract_u2u3_grid(float traj[DU3][DU2], float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12]){
	int j1, j3;
	float x2min, x2max, u2, u2min, u2max, u3, u3min, u3max, R, A ;
	float x3 = 0.3;
	int u1 = 0;

	x2min = fx2min();
	x2max = fx2max();
	u2min = fu2min();
	u2max = fu2max();
	u3min = fu3min();
	u3max = fu3max();

	for(j3=0;j3<DU3;j3++) {
        u3 = Valeur(j3,u3max,u3min,DU3);
        for(j1=0;j1<DU2;j1++) {
            u2 = Valeur(j1,u2max,u2min,DU2);
            R = R_nS(x3,u1,Assolements,u2);

            A = Attractiveness_evolution(0.5,u3,R);
            //printf("%f",A);
            traj[j3][j1] = Valeur(Indice(A,x2max,x2min,DX2),x2max,x2min,DX2) ;
        }
	}

    return 0;
}


void TestAttract(float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12]){
    int j1, j3, i2, u1;
	float x2, x2min, x2max, u3, u3min, u3max, u3mid, R, A, newA ;
	float x3 = 1.0;
	float u2 = U2MIN;
	int c = 0;

	x2min = fx2min();
	x2max = fx2max();
	u3min = fu3min();
	u3max = fu3max();
u3mid = U3MIN + (U3MAX-U3MIN)/2;

    for(j1=0;j1<NB_ASSOLEMENTS;j1++) {
        u1 = j1;
        for (i2=0;i2<DX2;i2++) {
            x2 = Valeur(i2,x2max,x2min,DX2);
            for(j3=0;j3<DU3;j3++) {
                u3 = Valeur(j3,u3max,u3min,DU3);
    R = R_nS(x3,u1,Assolements,u2);
    newA = Attractiveness_evolution(x2,u3,R);


    printf("%f\n",newA);

            }
        }
    }

}




int Evol_x1(float traj[DX2][O1_DR], float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12]){
	int j1, j3;
	float x2, x2min, x2max, R, A ;
	float x1 = 0.0;
	int u1 = 83;
	float u3 = 10.0;
	float u2 = 0.2;
	float RMAX = production_maximale();

	x2min = fx2min();
	x2max = fx2max();


	for(j3=0;j3<DX2;j3++) {
        x2 = Valeur(j3,x2max,x2min,DX2);
        for(j1=0;j1<O1_DR;j1++) {
            R = Valeur(j1,RMAX,0.0,O1_DR);

            A = x1+ GainRepas(x2,u3)-CoutAchatFoodExterieur(x2,R)-FIXED_COSTSR-Cout(u1, Assolements, u2);
            traj[j3][j1] = A ;
        }
	}

    return 0;
}

int Evol_x1_g(float traj[DX2][O1_DR], float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12]){
	int j1, j3;
	float x1min, x1max, x2, x2min, x2max, R, A ;
	float x1 = 0.0;
	int u1 = 83;
	float u3 = 15.0;
	float u2 = 0.2;
	float RMAX = production_maximale();

	x1min = fx1min();
	x1max = fx1max();
	x2min = fx2min();
	x2max = fx2max();


	for(j3=0;j3<DX2;j3++) {
        x2 = Valeur(j3,x2max,x2min,DX2);
        for(j1=0;j1<O1_DR;j1++) {
            R = Valeur(j1,RMAX,0.0,O1_DR);

            A = x1+ GainRepas(x2,u3)-CoutAchatFoodExterieur(x2,R)-FIXED_COSTSR-Cout(u1, Assolements, u2);

            traj[j3][j1] = Valeur(Indice(A,x1max,x1min,DX1),x1max,x1min,DX1) ;
        }
	}

    return 0;
}


int Gain(float res[DU3][DX2]){
    int j1, j3;
	float u3, u3min, u3max, x2, x2min, x2max, A ;


	u3min = fu3min();
	u3max = fu3max();
	x2min = fx2min();
	x2max = fx2max();


	for(j3=0;j3<DU3;j3++) {
        u3 = Valeur(j3,u3max,u3min,DU3);
        for(j1=0;j1<DX2;j1++) {
            x2 = Valeur(j1,x2max,x2min,DX2);

            A = GainRepas(x2,u3)-FIXED_COSTSR;

            res[j3][j1] = A ;
        }
	}
    return 0;
    }

int CoutsFoodExt(float res[O1_DR][DX2]){
    int j1, j3;
	float r, RMAX, x2, x2min, x2max, A ;


	RMAX = production_maximale();
	x2min = fx2min();
	x2max = fx2max();


	for(j3=0;j3<O1_DR;j3++) {
        r = Valeur(j3,RMAX,0.0,O1_DR);
        //printf("%f",r);
        for(j1=0;j1<DX2;j1++) {
            x2 = Valeur(j1,x2max,x2min,DX2);

            A = CoutAchatFoodExterieur(x2,r);

            res[j3][j1] = A ;
        }
	}



    return 0;
    }





int E_R(float E[DX3*NB_ASSOLEMENTS*DU2], float R[DX3*NB_ASSOLEMENTS*DU2], float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12]) {
    int i3, j1, j2, u1;
    float x3, x3min, x3max, u2, u2min, u2max;

    x3min = fx3min();
	x3max = fx3max();
	u2min = fu2min();
	u2max = fu2max();

    for (i3=0;i3<DX3;i3++) {
            x3 = Valeur(i3,x3max,x3min,DX3);
            for (j1=0;j1<NB_ASSOLEMENTS;j1++) {
                u1 = j1;
                for (j2=0;j2<DU2;j2++) {
                    u2 = Valeur(j2,u2max,u2min,DU2);
                    E[i3*NB_ASSOLEMENTS*DU2+j1*DU2+j2] = Cout(u1,Assolements,u2);
                    R[i3*NB_ASSOLEMENTS*DU2+j1*DU2+j2] = R_nS(x3,u1,Assolements,u2);
                }}}
    return 0;
}



int Evol_x3(float X3[NB_ASSOLEMENTS][DU2], float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12]){
           int j1, j2, u1;
           float u2, x3min, x3max, u2min, u2max;
           float x3 = 0.5;

           x3min = fx3min();
	x3max = fx3max();
	u2min = fu2min();
	u2max = fu2max();

           for (j1=0;j1<NB_ASSOLEMENTS;j1++) {
                u1 = j1;
                for (j2=0;j2<DU2;j2++) {
                    u2 = Valeur(j2,u2max,u2min,DU2);

                    X3[j1][j2] = BISQ_evol(x3,u1,Assolements,u2,x3min,x3max) - x3;
                }}

return 0;
}

int Evol_x3_g(float X3[NB_ASSOLEMENTS][DU2], float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12]){
           int j1, j2, u1;
           float u2, x3min, x3max, u2min, u2max;
           float x3 = 0.5;

           x3min = fx3min();
	x3max = fx3max();
	u2min = fu2min();
	u2max = fu2max();

           for (j1=0;j1<NB_ASSOLEMENTS;j1++) {
                u1 = j1;
                for (j2=0;j2<DU2;j2++) {
                    u2 = Valeur(j2,u2max,u2min,DU2);

                    X3[j1][j2] = Valeur(Indice(BISQ_evol(x3,u1,Assolements,u2,x3min,x3max),x3max,x3min,DX3),x3max, x3min, DX3) - x3;
                }}

return 0;
}







int main(int argc, char *argv[]){
    int b,i,j;
    float f, x2, x2min, x2max;
	float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12], R[DU2][NB_ASSOLEMENTS], res1[DU3][DU2], res2[DX2][O1_DR], res3[NB_ASSOLEMENTS][DU2], res4[DU3][DX2], res5[O1_DR][DX2], valE[DX3*NB_ASSOLEMENTS*DU2], valR[DX3*NB_ASSOLEMENTS*DU2];


	CreateAssolements(Assolements);




    b = CalculRdts(R,Assolements);

    printf("Yields =");
    for (i=0;i<NB_ASSOLEMENTS;i++){
            if (i==0) printf("[");
		else printf(",");
		for (j=0;j<DU2;j++){
			if (j==0) printf("[");
			else printf(",");
			printf("%f",R[j][i]);
			if (j==DU2-1) printf("]");
		}
		if (i==NB_ASSOLEMENTS-1) printf("]\n");
	}
//    x2max = fx2max();
//    f = NB_CLIENTS_MAX*x2max*FPP;
//    printf("Besoin max restau : %f",f);


//    x2min = fx2min();
//	x2max = fx2max();
//    printf("Besoins restau =");
//    for (i=0;i<DX2;i++){
//        if (i==0) printf("[");
//		else printf(",");
//		x2 = Valeur(i,x2max,x2min,DX2);
//        f = NB_CLIENTS_MAX*x2*FPP;
//        printf("%f",f);
//		if (i==DX2-1) printf("]");
//	}




// TestAttract(Assolements);




//	b = CalculEvolAttract_u2u3_grid(res1, Assolements);
//
//	printf("Evol Attract =");
//    for (i=0;i<DU3;i++){
//            if (i==0) printf("[");
//		else printf(",");
//		for (j=0;j<DU2;j++){
//			if (j==0) printf("[");
//			else printf(",");
//			printf("%f",res1[i][j]);
//			if (j==DU2-1) printf("]");
//		}
//		if (i==DU3-1) printf("]");
//	}





//    printf("Prod max = %f kg\n", production_maximale());
//
//b = Evol_x1_g(res2,Assolements);
//
//    printf("Evolution of the cumulative cash flow (x1) =");
//    for (i=0;i<DX2;i++){
//            if (i==0) printf("[");
//		else printf(",");
//		for (j=0;j<O1_DR;j++){
//			if (j==0) printf("[");
//			else printf(",");
//			printf("%f",res2[i][j]);
//			if (j==O1_DR-1) printf("]");
//		}
//		if (i==DX2-1) printf("]");
//	}



//b= Gain(res4);
//
//    printf("Evolution restaurant's gains =");
//    for (i=0;i<DU3;i++){
//            if (i==0) printf("[");
//		else printf(",");
//		for (j=0;j<DX2;j++){
//			if (j==0) printf("[");
//			else printf(",");
//			printf("%f",res4[i][j]);
//			if (j==DX2-1) printf("]");
//		}
//		if (i==DU3-1) printf("]");
//	}

b= CoutsFoodExt(res5);

    printf("Cost of food purchase from external producers =");
    for (i=0;i<DU3;i++){
            if (i==0) printf("[");
		else printf(",");
		for (j=0;j<DX2;j++){
			if (j==0) printf("[");
			else printf(",");
			printf("%f",res5[i][j]);
			if (j==DX2-1) printf("]");
		}
		if (i==DU3-1) printf("]");
	}


//	b = E_R(valE,valR,Assolements);
//
//	FILE* fE = NULL ;
//	fE = fopen("DataE.txt", "w");
//	if (fE != NULL){
//
//        fprintf(fE,"Depenses (EUR) = [");
//    for (i=0;i<DX3*NB_ASSOLEMENTS*DU2;i++){
//        if (i>0) fprintf(fE,",");
//        fprintf(fE,"%f",valE[i]);
//    }
//	fprintf(fE,"]\n");
//
//        fclose(fE);
//	}
//	else {
//        printf("fail (E)");
//	}
//
//	FILE* fR = NULL ;
//	fR = fopen("DataR.txt", "w");
//	if (fR != NULL){
//
//        fprintf(fR,"Production (kg) = [");
//    for (i=0;i<DX3*NB_ASSOLEMENTS*DU2;i++){
//        if (i>0) fprintf(fR,",");
//        fprintf(fR,"%f",valR[i]);
//    }
//	fprintf(fR,"]\n");
//
//        fclose(fR);
//	}
//	else {
//        printf("fail (R)");
//	}







//    b = Evol_x3_g(res3,Assolements);
//
//    printf("Evolution of soil quality index (x3) =");
//    for (i=0;i<NB_ASSOLEMENTS;i++){
//            if (i==0) printf("[");
//		else printf(",");
//		for (j=0;j<DU2;j++){
//			if (j==0) printf("[");
//			else printf(",");
//			printf("%f",res3[i][j]);
//			if (j==DU2-1) printf("]");
//		}
//		if (i==NB_ASSOLEMENTS-1) printf("]");
//	}


    return 0;
}
