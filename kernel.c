#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include "organisation1.h"
#include "organisation2.h"
#include "utils.h"
#include "dynamics.h"


// This script enables to compute one set of kernels (A+R). Rmin and Emax values have to be given as inputs at execution.



int main(int argc, char *argv[]){
	int b,i,j;
	float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12];
    int Intx3frontiere[DX3],Intx1frontiere[DX2];
	float Rmin,Emax,Emin;
	int DR = 3;
    int DE = 4;

	float RMAX = production_maximale();
	float EMAX = cout_maximal();

	printf("Minimal production (kg/year) : ");
	scanf("%f", &Rmin);
//	printf("Maximal expenses (euro/year) : ");
//	scanf("%f", &Emax);
//
//	O1_CalculNoyauAgricole1D(Intx3frontiere,Rmin,Emax);
//
//	O1_CalculFrontiereNoyauRestaurant2D(Intx1frontiere,RMAX,Rmin,Emax,0,DR,DE);


    printf("Minimal gain (euro/an) : ");
	scanf("%f", &Emin);

	O2_CalculNoyauAgricole2D(Intx3frontiere,Rmin,Emin,EMAX,DE);

	O2_CalculFrontiereNoyauRestaurant1D(Intx1frontiere,RMAX,Rmin,EMAX,Emin,DR);

	printf("\n AGRICULTURE\n");
	for (i=0;i<DX3;i++){
		if (i==0) printf("[");
		else printf(",");
        printf("%d",Intx3frontiere[i]);
		if (i==DX3-1) printf("]");
    }


	printf("\n RESTAURANT\n");
	for (i=0;i<DX2;i++){
		if (i==0) printf("[");
		else printf(",");
        printf("%d",Intx1frontiere[i]);
		if (i==DX2-1) printf("]");
	}




//	FILE* sauv = NULL ;
//	sauv = fopen("Results.txt", "a");
//	if (sauv != NULL){
//        InscriptionParametres(sauv);
//        InscriptionResultats(sauv, surfaces_Agricole1D, surfaces_Restaurant2D);
//        fclose(sauv);
//	}
//	else {
//        printf("WARNING : File saving failed !");
//	}

	return 0;
}

