#ifndef _dynamics_H_
#define _dynamics_H_

#include "init.h"

//Parameters of the dynamics
//For x1
#define GAIN 0.7 // share of the raw income that do not include variable costs (excluding fixed costs)
#define FIXED_COSTSR 5000*12 // includes cookers salaries and equipment depreciation 
#define FOOD_PRICE 1.5 // mean price in € of 1 kg of wholesale food
#define PRICE_MTX 2.0 // mean price at which the restaurant buys food from local production

#define FIXED_COSTSA 8000 + 2000*12

//For x2
#define NB_CLIENTS_MAX 75*180 // maximal capacity of the number of meals served each year
#define FPP  0.8 // food per plate = the quantity of food in one plate (in kg)

// For x3


#define THR_WORKLOAD   4*35 // maximal monthly workload (one person)
#define RAP_FALLOW 0.025
#define WASTE 0.05 // proportion of food waste between field and fork (storage issues...)
#define R_MID 0.8 // Potential relative production, for which the effect on restaurant's attractivity is neutral.

// Parameters for the constraints
#define X3QUAL 0.2
#define X1V 0.0


// For the trajectories
#define TMAX 20


//Computing x1 dynamics

//Computing restaurant's gains

float GainRepas(float x2,float u3);
float CoutAchatFoodExterieur(float x2,float Rn_x3_u1);
float Capital_evolutionR(float x2,float u3,float Rn_x3_u1);

//Computing agricultural expenses


float Cout(int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2);
float Capital_evolutionA(float Rn_x3_u1,int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2);

//Computing x2 dynamics

// Evolution of the attractivity coefficient


float Attractiveness_evolution(float x2,float u3,float Rn_x3_u1);

//Computing x3 dynamics


//Yields
float R_n(float x3,float RM,float ri);

float R_nS(float x3, int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2);



float production_maximale();

float cout_maximal();


// evolution of soil quality


float BISQ_evol(float x3, int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2,float x3min,float x3max);

#endif

