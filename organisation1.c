#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "organisation1.h"
#include "dynamics.h"
#include "utils.h"
//#include "init.h"

//************************

// The splitting of the system can be conceived in several ways.

// Organization O1: Farm activity is only concerned by keeping good soil quality: x3 >=x3QUAL. Only x3 can be seen and 2 controls can be used, U1 and u2. The restaurant is concerned by keeping global cumulative cash flows positive: x1>=0 using control u3
// Negotiations between the two activities are about commitments on yearly crop production volume Rn_x3_u1 >= Rmin ; and a maximal cost for this production expenses<=Emax.
// The aim is to compute the guaranteed viability kernels of both activities for various commitments, in other words various values for Rmin and Emax.


//************************

// Computation of the farmer's guaranteed viability kernel: 1 state dimension x3, two control dimensions u1 and u2, one constraint plus two commitments. 


int O1_IsinConstraintSetAgricole1D(float x3){
	/// Verifies that x3 respects the constraint.
	int b=0;
	if (x3>=X3QUAL) b = 1;
	return b;
}



int O1_IsinAdmissibleControlSetAgricole1D(float x3,int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2,float Rmin,float Emax){
	/// This function verifies that the couple of controls (u1, u2) is allowed with a state of the system x3 given the two commitments made.
	int b=0;
	if ((Cout(u1,Assolements,u2)<=Emax)&&(R_nS(x3,u1,Assolements,u2)>=Rmin)) b = 1;

	return b;
}

//x3 dynamics:
float O1_fx3next(float x3,int u1,float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12],float u2,float x3min,float x3max){
	float x3next;
	x3next = BISQ_evol(x3,u1,Assolements,u2,x3min,x3max);
	return x3next;
}

void O1_CalculNoyauAgricole1D(int Intx3frontiere[DX3],float Rmin,float Emax){
	float x3min,x3max, u2min,u2max, x3,x3next,u2;
	float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12];
	int change,passage,i,b,ii,jj,u1,intx3next;


//Initialization of exploration bounds
	x3min = fx3min();
	x3max= fx3max();
	b = CreateAssolements(Assolements);
	u2min = fu2min();
	u2max = fu2max();

// Initialization of the variable indicating if a change was made
	change=1;
	passage = 1;


// Initialization of the tables storing the kernel values: 1 if viable, 0 else

	for(i=0;i<DX3;i++) {
		x3= Valeur(i,x3max,x3min,DX3);
		if (O1_IsinConstraintSetAgricole1D(x3)) Intx3frontiere[i]=1;
		else Intx3frontiere[i]=0;
	}

//CComputation
	while(change>0){

		change=0;

		passage = passage + 1;
		for(i=0;i<DX3;i++){
			if (Intx3frontiere[i] == 1){
				x3= Valeur(i,x3max,x3min,DX3);

				if (O1_IsinConstraintSetAgricole1D(x3)){

					b = 0;
					for(ii=0;ii<NB_ASSOLEMENTS;ii++) {
						u1 = ii;
		
						for(jj=0;jj<DU2;jj++) {
							u2 = Valeur(jj,u2max,u2min,DU2);
				
							if (O1_IsinAdmissibleControlSetAgricole1D(x3,u1,Assolements,u2,Rmin,Emax)){

								x3next = O1_fx3next(x3,u1,Assolements,u2,x3min,x3max);
								if (O1_IsinConstraintSetAgricole1D(x3next)){
									intx3next = Indice(x3next,x3max, x3min,DX3);
									if (Intx3frontiere[intx3next]==1){
										b = 1;
										break;
									}
								}
							}
						}
						if (b ==1){
							break;
						}
					}
					if (b == 0){
						Intx3frontiere[i] = 0;
						change = change + 1;
					}
				}
				else 	Intx3frontiere[i] = 0;

			}
		}

	}

}

// Computation of the restaurant's guaranteed viability kernel: 2 state dimensions x1 and x2, 1 control u3, one constraint, no commitment. 

int O1_IsinConstraintSetREstaurant2D(float x1,float x2){
	/// Verifies that (x1, x2) respects the constraint.
	int b=0;
	if (x1>=0) b = 1;
	return b;
}


int O1_IsinAdmissibleControlSetRestaurant2D(float x1,float x2,float u3){
	/// NO commitment.
	int b=1;
	return b;
}

//Dynamics of x1 and x2

float O1_fx1next(float x1,float x2,float u3,float Rn_x3_u1,float E){
	float x1next;
	x1next = x1+ GainRepas(x2,u3)-CoutAchatFoodExterieur(x2,Rn_x3_u1)-E-FIXED_COSTSR;
	return x1next;
}

float O1_fx2next(float x2,float u3,float Rn_x3_u1){
	float x2next;
	x2next = Attractiveness_evolution(x2,u3,Rn_x3_u1);
	return x2next;
}

// Computation of the lower border of the guaranteed viability kernel as a table Intx2frontiere[DX2] associating attractivity values (from X2MIN to X2MAX) with the index of the lowest value of cumulative cash flow x1 belonging to the guaranteed viability kernel.
// Here, the property of linearity with x1 is used to transform a 2D-kernel into a 1D-table.

void O1_CalculFrontiereNoyauRestaurant2D(int Intx1frontiere[DX2],float Rmax,float Rmin,float Emax,float Emin,int DR,int DE){
	float x1min,x1max,x2min,x2max,u3min,u3max,v1min,v1max,v2min,v2max,x1,x2,u3,x1next,x2next,E,Rn_x3_u1;
	int change,passage,i,j,b,ii,jj,iii,jjj,intx2next;


//Initialization of exploration bounds
	x1min = fx1min();
	x1max= fx1max();
	x2min = fx2min();
	x2max= fx2max();
	u3min = fu3min();
	u3max = fu3max();

// Initialization of the variable storing if a change was made
	change=1;
	passage = 1;

//Initialization of the table storing the kernel

	for(i=0;i<DX2;i++) {
		Intx1frontiere[i]=0;
	}

//Computation
	while(change>0){

		change=0;

		passage = passage + 1;
		for(i=0;i<DX2;i++){
			x2= Valeur(i,x2max,x2min,DX2);

			for(j=Intx1frontiere[i];j<DX1;j++){
				b = 0;
				x1 = Valeur(j,x1max,x1min,DX1);

				if (O1_IsinConstraintSetREstaurant2D(x1,x2)){

					for(ii=0;ii<DU3;ii++) {
						u3 = Valeur(ii,u3max,u3min,DU3);
		
						if (O1_IsinAdmissibleControlSetRestaurant2D(x1,x2,u3)){
			
							b = 1;
							for(iii=0;iii<DR;iii++) {
								Rn_x3_u1 = Valeur(iii,Rmax,Rmin,DR);
								x2next = O1_fx2next(x2,u3,Rn_x3_u1);
								for(jjj=0;jjj<DE;jjj++) {
									E = Valeur(jjj,Emax,Emin,DE);
									x1next = O1_fx1next(x1,x2,u3,Rn_x3_u1,E);
									if (O1_IsinConstraintSetREstaurant2D(x1next,x2next)){
										intx2next = Indice(x2next,x2max, x2min,DX2);
										if (x1next < Valeur(Intx1frontiere[intx2next],x1max,x1min,DX1) || Intx1frontiere[intx2next] == DX1){
											b = 0;
											break;
										}
									}
									else{
										b = 0;
										break;
									}
								}
								if(b == 0){
									break;
								}
							}
						}
						if(b == 1){
							break;
						}
					}
				}
				if (b == 1){
					Intx1frontiere[i] = j;
					break;
				}
				else {
					change = change + 1;
				}
			}
			if (b == 0){
				Intx1frontiere[i] = DX1;
			}
		}

	}

}


// Computation of the kernel's size
float O1_CalculSurfacenoyauAgricole1D(int Intx3frontiere[DX3]){
	float surface;
	int i;
	surface = 0;
	for(i=0;i<DX3;i++) {
		if (Intx3frontiere[i] == 1) surface=surface+1;
	}
	surface = surface/(DX3);
	return surface;
}

float O1_CalculSurfacenoyauRestaurant2D(int Intx1frontiere[DX2]){
	float surface;
	int i;
	surface = 0;
	for(i=0;i<DX2;i++) {
		surface=surface+(float)(DX1)-(float)(Intx1frontiere[i]);
	}
	surface = surface/(DX1*DX2);
	return surface;
}

float O1_CalculSurfacenoyauProduit(int Intx3frontiere[DX3], int Intx1frontiere[DX2]){
	float surface;
	int i, j;
	surface = 0;
	for(i=0;i<DX3;i++) {
        if (Intx3frontiere[i] == 1){
            for(j=0;j<DX2;j++) {

                surface=surface+(float)(DX1)- (float)(Intx1frontiere[j]);
	}}}
	surface = surface/(DX1*DX2*DX3);
	return surface;
}



int O1_CalculTrajectoireViable(float x10,float x20,float x30, int Intx3frontiere[DX3],int Intx1frontiere[DX2],float traj_x1[TMAX],float traj_x2[TMAX],float traj_x3[TMAX],int traj_u1[TMAX],float traj_u2[TMAX],float traj_u3[TMAX]){
	int b=0;
	int t=0;
	int j1,j2,j3,u1,intx1current,intx2current,intx3current;
	float u2,u3,x1current,x2current,x3current,x1,x2,x3,x1next,x2next,x3next, R, E;
	float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12];
	float x1min,x1max,x2min,x2max,x3min,x3max,u2min,u2max,u3min,u3max;

	if (O1_IsinConstraintSetAgricole1D(x30) && O1_IsinConstraintSetREstaurant2D(x10, x20)){

		x1min = fx1min();
		x1max= fx1max();
		x2min = fx2min();
		x2max= fx2max();
		u3min = fu3min();
		u3max = fu3max();
		x3min = fx3min();
		x3max= fx3max();
		CreateAssolements(Assolements);
		u2min = fu2min();
		u2max = fu2max();
		x1current = x10;
		x2current = x20;
		x3current = x30;
		while (t<TMAX){
			//printf("xcurrent %f %f %f\n",x1current,x2current,x3current);
			printf(" t %d\n",t);
			b = 0;
			x1 = Valeur(Indice(x1current,x1max, x1min,DX1),x1max, x1min,DX1);
			x2 = Valeur(Indice(x2current,x2max, x2min,DX2),x2max, x2min,DX2);
			x3 = Valeur(Indice(x3current,x3max, x3min,DX3),x3max, x3min,DX3);


            for(j2=0;j2<DU2;j2++) {
                u2 = Valeur(j2,u2max,u2min,DU2);
                for(j3=0;j3<DU3;j3++) {
                    u3 = Valeur(j3,u3max,u3min,DU3);
                    for(j1=0;j1<NB_ASSOLEMENTS;j1++) {
                        u1 = j1;
                        R = R_nS(x3,u1,Assolements,u2);
                        E = Cout(u1,Assolements,u2);
                        x1next = O1_fx1next(x1,x2,u3,R,E);
                        x2next = O1_fx2next(x2,u3,R);
                        x3next = O1_fx3next(x3,u1,Assolements,u2,x3min,x3max);

                        if ((x1next > Valeur(Intx1frontiere[Indice(x2next,x2max, x2min,DX2)],x1max,x1min,DX1)) && Intx1frontiere[Indice(x2next,x2max, x2min,DX2)] < DX1 && (Intx3frontiere[Indice(x3next,x3max, x3min,DX3)] == 1)) {
                            b = 1; // viable

                            break;
                            }

                        }
                        if (b ==1) break;
                    }
                    if (b ==1) break;

			}
			if (b==0) {printf("problem");getchar();}
			traj_x1[t] = x1;
			traj_x2[t] = x2;
			traj_x3[t] = x3;
			traj_u1[t] = u1;
			traj_u2[t] = u2;
			traj_u3[t] = u3;
			x1current = x1next;
			x2current = x2next;
			x3current = x3next;
			t =t+1;
		}
	}
	else{
        printf("Starting point is not viable.");
	}
	return b;
}

void shuffle_ctrls(int u1[NB_ASSOLEMENTS], int u2[DU2], int u3[DU3]){
    /// Shuffles a list of size TMAX composed by integers.
    int i, j, temp;

    srand(time(NULL));
    for (i = 0; i < NB_ASSOLEMENTS; ++i){
        j = rand() % (NB_ASSOLEMENTS-i) + i;
        temp = u1[i];
        u1[i] = u1[j];
        u1[j] = temp;
}

    for (i = 0; i < DU2; ++i){
            j = rand() % (DU2-i) + i;
            temp = u2[i];
            u2[i] = u2[j];
            u2[j] = temp;
    }
    for (i = 0; i < DU3; ++i){
            j = rand() % (DU3-i) + i;
            temp = u3[i];
            u3[i] = u3[j];
            u3[j] = temp;
    }
}


int O1_CalculTrajectoireViableAlea(float x10,float x20,float x30, int Intx3frontiere[DX3],int Intx1frontiere[DX2],float traj_x1[TMAX],float traj_x2[TMAX],float traj_x3[TMAX],int traj_u1[TMAX],float traj_u2[TMAX],float traj_u3[TMAX]){
    /// Computes a random trajectory.
	int b=0;
	int t=0;
	int j1,j2,j3,u1,intx1current,intx2current,intx3current;
	float u2,u3,x1current,x2current,x3current,x1,x2,x3,x1next,x2next,x3next, R, E;
	float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12];
	float x1min,x1max,x2min,x2max,x3min,x3max,u2min,u2max,u3min,u3max;
	int indu1[NB_ASSOLEMENTS], indu2[DU2], indu3[DU3];

	if (O1_IsinConstraintSetAgricole1D(x30) && O1_IsinConstraintSetREstaurant2D(x10, x20)){

		x1min = fx1min();
		x1max= fx1max();
		x2min = fx2min();
		x2max= fx2max();
		u3min = fu3min();
		u3max = fu3max();
		x3min = fx3min();
		x3max= fx3max();
		CreateAssolements(Assolements);
		u2min = fu2min();
		u2max = fu2max();
		x1current = x10;
		x2current = x20;
		x3current = x30;

        for (j1=0; j1<NB_ASSOLEMENTS; j1++){indu1[j1] = j1;}
		for (j2=0; j2<DU2; j2++){indu2[j2] = j2;}
		for (j3=0; j3<DU3; j3++){indu3[j3] = j3;}

		while (t<TMAX){
			//printf("xcurrent %f %f %f\n",x1current,x2current,x3current);
			printf(" t %d\n",t);
			b = 0;
			x1 = Valeur(Indice(x1current,x1max, x1min,DX1),x1max, x1min,DX1);
			x2 = Valeur(Indice(x2current,x2max, x2min,DX2),x2max, x2min,DX2);
			x3 = Valeur(Indice(x3current,x3max, x3min,DX3),x3max, x3min,DX3);


			shuffle_ctrls(indu1, indu2, indu3);

            for(j3=0;j3<DU3;j3++) {
                u3 = Valeur(indu3[j3],u3max,u3min,DU3);
                for(j2=0;j2<DU2;j2++) {
                    u2 = Valeur(indu2[j2],u2max,u2min,DU2);
                    for(j1=0;j1<NB_ASSOLEMENTS;j1++) {
                        u1 = indu1[j1];
                        R = R_nS(x3,u1,Assolements,u2);
                        E = Cout(u1,Assolements,u2);
                        x1next = O1_fx1next(x1,x2,u3,R,E);
                        x2next = O1_fx2next(x2,u3,R);
                        x3next = O1_fx3next(x3,u1,Assolements,u2,x3min,x3max);

                        if ((x1next > Valeur(Intx1frontiere[Indice(x2next,x2max, x2min,DX2)],x1max,x1min,DX1)) && Intx1frontiere[Indice(x2next,x2max, x2min,DX2)] < DX1 && (Intx3frontiere[Indice(x3next,x3max, x3min,DX3)] == 1)) {
                            b = 1; // viable

                            break;
                            }

                        }
                        if (b ==1) break;
                    }
                    if (b ==1) break;

			}
			if (b==0) {printf("problem");getchar();}
			traj_x1[t] = x1;
			traj_x2[t] = x2;
			traj_x3[t] = x3;
			traj_u1[t] = u1;
			traj_u2[t] = u2;
			traj_u3[t] = u3;
			x1current = x1next;
			x2current = x2next;
			x3current = x3next;
			t =t+1;
		}
	}
	else{
        printf("Starting point is not viable.");
	}
	return b;
}
