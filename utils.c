//*********************** These functions make the connection between an index value and the corresponding real value in the grid, and vice versa *****************************************


int Indice(float x,float xmax, float xmin,int Dx){
	/// Function returning the index associated to a real value, given the bounds and precision D
	int ix;
	ix=(int)((x-xmin)*(Dx-1)/(xmax-xmin));
	return fminf(ix,Dx);
}



float Valeur(int ix,float xmax, float xmin,int Dx){
	/// Function returning the value associated to the index.
	float x;
	x=xmin+(xmax-xmin)*(float)(ix)/(Dx-1);
	return x;
}

